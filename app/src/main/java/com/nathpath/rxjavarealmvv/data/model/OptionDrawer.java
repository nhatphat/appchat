package com.nathpath.rxjavarealmvv.data.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.view.fragment.option.HomeFragment;

public class OptionDrawer {
    private @DrawableRes int icon;
    private @StringRes int title;
    private BaseFragment content;
    private boolean Selected = false;

    public OptionDrawer() {
        this.icon = R.drawable.ic_option_default;
        this.title = R.string.unknown;
        this.content = new HomeFragment();
    }

    public OptionDrawer(int icon, int title, BaseFragment content) {
        this.icon = icon;
        this.title = title;
        this.content = content;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public BaseFragment getContent() {
        return content;
    }

    public void setContent(BaseFragment content) {
        this.content = content;
    }

    public boolean getSelected(){
        return this.Selected;
    }

    public void setSelected(boolean selected) {
        this.Selected = selected;
    }
}
