package com.nathpath.rxjavarealmvv.data.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout user;
    public TextView tvUserStatus;
    public ImageView imgUserState;
    public TextView tvUserDislayName;
    public CircleImageView civUserAvatar;

    public View currentView;

    public UserViewHolder(View itemView) {
        super(itemView);
        currentView = itemView;

        user = itemView.findViewById(R.id.user);
        imgUserState = itemView.findViewById(R.id.imgUserState);
        civUserAvatar = itemView.findViewById(R.id.civUserAvatar);
        tvUserDislayName = itemView.findViewById(R.id.tvUserDisplayName);
        tvUserStatus = itemView.findViewById(R.id.tvUserStatus);
    }
}
