package com.nathpath.rxjavarealmvv.data.remote;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit mRetrofit;

    public static Retrofit getClient(String url){
        if(mRetrofit == null){
            mRetrofit = new Retrofit.Builder()
                                    .baseUrl(url)
                                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
        }
        return mRetrofit;
    }
}
