package com.nathpath.rxjavarealmvv.data.model;

import android.support.annotation.NonNull;

public class Contact implements Comparable {
    private String name;
    private String phoneNumber;

    public Contact() {
        this.name = "noname";
        this.phoneNumber = "noPhone";
    }

    public Contact(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if (o instanceof Contact) {
            Contact contact = (Contact) o;
                return this.name.compareTo(contact.name);
        }
        return -1;
    }
}
