package com.nathpath.rxjavarealmvv.data.model;

import java.io.Serializable;

public class Message implements Serializable{
    private String key;
    private String from;//don't rename
    private String message;//don't rename
    private String time;//don't rename

    public Message() {}

    public Message(String from, String message, String time) {
        this.from = from;
        this.message = message;
        this.time = time;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
