package com.nathpath.rxjavarealmvv.data.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.view.fragment.error.ErrorFragment;

public class PageContent {
    private @DrawableRes int iconPageSelect = R.drawable.ic_tag;
    private @DrawableRes int iconPageNone = R.drawable.ic_tag_none;
    private @DrawableRes int backgroundForAppbarLayout = R.drawable.gradient_ocean_blue;
    private @StringRes int titlePage = R.string.unknown;
    private BaseFragment fragmentPage;

    public PageContent(){
        this.fragmentPage = new ErrorFragment();
    }

    public PageContent(int titlePage, BaseFragment fragmentPage) {
        this.titlePage = titlePage;
        this.fragmentPage = fragmentPage;
    }

    public PageContent(BaseFragment fragmentPage) {
        this.fragmentPage = fragmentPage;
    }

    public PageContent(int iconPageNone, int iconPageSelect, int backgroundForAppbarLayout, int titlePage, BaseFragment fragmentPage) {
        this.iconPageSelect = iconPageSelect;
        this.iconPageNone = iconPageNone;
        this.backgroundForAppbarLayout = backgroundForAppbarLayout;
        this.titlePage = titlePage;
        this.fragmentPage = fragmentPage;
    }

    public int getIconPageSelect() {
        return iconPageSelect;
    }

    public void setIconPageSelect(int iconPageSelect) {
        this.iconPageSelect = iconPageSelect;
    }

    public int getIconPageNone() {
        return iconPageNone;
    }

    public void setIconPageNone(int iconPageNone) {
        this.iconPageNone = iconPageNone;
    }

    public int getBackgroundForAppbarLayout(){
        return this.backgroundForAppbarLayout;
    }

    public void setBackgroundForAppbarLayout(int backgroundForAppbarLayout){
        this.backgroundForAppbarLayout = backgroundForAppbarLayout;
    }

    public int getTitlePage() {
        return titlePage;
    }

    public void setTitlePage(int titlePage) {
        this.titlePage = titlePage;
    }

    public BaseFragment getFragmentPage() {
        return fragmentPage;
    }

    public void setFragmentPage(BaseFragment fragmentPage) {
        this.fragmentPage = fragmentPage;
    }
}
