package com.nathpath.rxjavarealmvv.data.model;

import java.io.Serializable;

public class User implements Serializable {
    private boolean state;//don't rename
    private String avatar;//don't rename
    private String thumb_avatar;//don't rename
    private String display_name;//don't rename
    private String email;//don't rename
    private String status;//don't rename

    public User() {}

    public User(String avatar, String thumb_avatar, String display_name, String email, String status) {
        this.avatar = avatar;
        this.thumb_avatar = thumb_avatar;
        this.display_name = display_name;
        this.email = email;
        this.status = status;
    }

    public boolean getState() {
        return this.state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getThumb_avatar() {
        return thumb_avatar;
    }

    public void setThumb_avatar(String thumb_avatar) {
        this.thumb_avatar = thumb_avatar;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
