package com.nathpath.rxjavarealmvv.presenter.content;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.callback.RequestFragmentView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RequestFragmentPresenter {
    private String ERROR = "Error connect";
    private RequestFragmentView mCallBack;

    public RequestFragmentPresenter(RequestFragmentView mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void loadRequest(DatabaseReference databaseReference, AuthManager authManager) {
        if (mCallBack != null && authManager.infoUserAvailable()) {
            String currentUserId = authManager.getU_Id();
            databaseReference.child(Common.REQUEST_NODE).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.hasChild(currentUserId)){
                        load_request_from(databaseReference, authManager);
                    }else{
                        mCallBack.onLoadRequestSuccess(null, null, true);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    mCallBack.onFailed(databaseError.getMessage());
                }
            });

        }
    }

    private void load_request_from(DatabaseReference databaseReference, AuthManager authManager){
        List<User> data = new ArrayList<>();
        List<String> id = new ArrayList<>();
        String currentUserId = authManager.getU_Id();
        databaseReference.child(Common.REQUEST_NODE)
                .child(currentUserId)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        if (dataSnapshot.exists()) {
                            if (dataSnapshot.getKey() != null) {
                                id.add(dataSnapshot.getKey());
                                databaseReference.child(Common.USER_NODE)
                                        .child(dataSnapshot.getKey())
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                User user = dataSnapshot.getValue(User.class);
                                                if (user != null) {
                                                    data.add(user);
                                                    mCallBack.onLoadRequestSuccess(data, id, false);
                                                } else {
                                                    Log.e("error", "request user null");
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                mCallBack.onFailed(databaseError.getMessage());
                                            }
                                        });
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        loadRequest(databaseReference, authManager);
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        mCallBack.onFailed(databaseError.getMessage());
                    }
                });
    }

    public void acceptRequest(DatabaseReference databaseReference, AuthManager authManager, String user_id) {
        if (mCallBack != null && authManager.infoUserAvailable()) {
            String currentUserId = authManager.getU_Id();
            String time = DateFormat.getDateTimeInstance().format(new Date());
            databaseReference.child(Common.FRIENDS_NODE)
                    .child(currentUserId)
                    .child(user_id)
                    .setValue(time)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            databaseReference.child(Common.FRIENDS_NODE)
                                    .child(user_id)
                                    .child(currentUserId)
                                    .setValue(time)
                                    .addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            mCallBack.onAcceptRequestSuccess(user_id);
                                        } else {
                                            mCallBack.onFailed(ERROR);
                                        }
                                    });
                        } else {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }

    public void cancelRequest(DatabaseReference databaseReference, AuthManager authManager, String user_id, boolean becomeFriend) {
        if (mCallBack != null && authManager.infoUserAvailable()) {
            String currentUserId = authManager.getU_Id();
            databaseReference.child(Common.REQUEST_NODE)
                    .child(currentUserId)
                    .child(user_id)
                    .removeValue()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mCallBack.onCancelRequestSuccess(becomeFriend);
                        } else {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }
}
