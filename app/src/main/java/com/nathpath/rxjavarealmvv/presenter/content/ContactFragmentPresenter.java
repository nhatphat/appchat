package com.nathpath.rxjavarealmvv.presenter.content;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.provider.ContactsContract;

import com.nathpath.rxjavarealmvv.data.model.Contact;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.callback.ContactFragmentView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ContactFragmentPresenter {
    private Context context;
    private static Handler handler;
    private static Runnable run;

    private ContactFragmentView mCallBack;

    public ContactFragmentPresenter(Context context, ContactFragmentView mCallBack) {
        this.mCallBack = mCallBack;
        this.context = context;
    }

    public void loadContact() {
        if (handler == null) {
            handler = new Handler();
        }

        if (run == null) {
            run = () -> {
                Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                if (phones != null) {
                    List<Contact> list = new ArrayList<>();
                    while (phones.moveToNext()) {
                        String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        list.add(new Contact(name, phoneNumber));
                    }
                    phones.close();
                    if(mCallBack != null){
                        mCallBack.loadContactSuccess(list);
                    }
                }else{
                    if(mCallBack != null){
                        mCallBack.loadContactFailed("");
                    }
                }
            };
        }

        handler.postDelayed(run, 1000);
    }
}
