package com.nathpath.rxjavarealmvv.presenter.profile;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.view.callback.ProfileFragmentView;

public class ProfileFragmentPresenter implements AuthManager.OnUpdateStateListener{
    private Context context;
    private ProfileFragmentView mCallBack;
    private AuthManager authManager;

    public ProfileFragmentPresenter(Context context, AuthManager authManager, ProfileFragmentView view) {
        this.context = context;
        mCallBack = view;
        this.authManager = authManager;
    }

    public void updateInfoCurrentUser(@Nullable String displayName, @Nullable String status, @Nullable Uri uriPhoto) {
        authManager.setOnUpdateStateListener(this);
        authManager.updateInfoCurrentUser(displayName, status, uriPhoto);
    }

    @Override
    public void onUpdateSuccess() {
        if (mCallBack != null) {
            mCallBack.onUpdateSuccess(context.getString(R.string.updated));
        }
    }

    @Override
    public void onUpdateFailed() {
        if (mCallBack != null) {
            mCallBack.onUpdateFailed(context.getString(R.string.update_failed));
        }
    }
}