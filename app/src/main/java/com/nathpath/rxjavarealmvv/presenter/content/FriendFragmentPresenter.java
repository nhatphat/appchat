package com.nathpath.rxjavarealmvv.presenter.content;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.nathpath.rxjavarealmvv.data.model.Friend;
import com.nathpath.rxjavarealmvv.data.model.Message;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.callback.FriendFragmentView;
import com.nathpath.rxjavarealmvv.view.fragment.content.ConversationFragment;

import java.util.ArrayList;
import java.util.List;

public class FriendFragmentPresenter {
    private FriendFragmentView mCallBack;

    public FriendFragmentPresenter(FriendFragmentView mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void loadFriendList(DatabaseReference databaseReference, AuthManager authManager) {
        if (mCallBack != null) {
            if (authManager.infoUserAvailable()) {
                String currentUserId = authManager.getU_Id();
                databaseReference.child(Common.FRIENDS_NODE).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(currentUserId)) {
                            loadFriendFrom(databaseReference, authManager);
                        } else {
                            mCallBack.onLoadFriendListSuccess(null, null, true);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        mCallBack.onFailed(databaseError.getMessage());
                    }
                });
            } else {
                Log.e("error", "load friend list: get uid k dk");
            }
        }
    }

    private void loadFriendFrom(DatabaseReference databaseReference, AuthManager authManager) {
        List<User> mData = new ArrayList<>();
        List<String> idList = new ArrayList<>();
        String currentUserId = authManager.getU_Id();
        databaseReference.child(Common.FRIENDS_NODE)
                .child(currentUserId)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        String userId = dataSnapshot.getKey();
                        if (userId != null) {
                            databaseReference.child(Common.USER_NODE)
                                    .child(userId)
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            User friend = dataSnapshot.getValue(User.class);
                                            if (friend != null) {
                                                String nodeMessage = ConversationFragment.createDataMessageNode(userId, currentUserId);
                                                databaseReference.child(Common.MESSAGE_NODE)
                                                        .child(nodeMessage)
                                                        .limitToLast(1)
                                                        .addChildEventListener(new ChildEventListener() {
                                                            @Override
                                                            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                                                Message mess = dataSnapshot.getValue(Message.class);
                                                                if (mess != null) {
                                                                    friend.setStatus(mess.getMessage());
                                                                } else {
                                                                    friend.setStatus("");
                                                                }
                                                                idList.add(userId);
                                                                mData.add(friend);
                                                                mCallBack.onLoadFriendListSuccess(mData, idList, false);
                                                            }

                                                            @Override
                                                            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                                            }

                                                            @Override
                                                            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                                                            }

                                                            @Override
                                                            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
                                            } else {
                                                Log.e("error", "load friend list: friend nuull nha");
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            mCallBack.onFailed(databaseError.getMessage());
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        loadFriendList(databaseReference, authManager);
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        mCallBack.onFailed(databaseError.getMessage());
                    }
                });
    }

    public void deleteAllMessage(DatabaseReference databaseReference, String userId, String yourId) {
        String nodeMessage = ConversationFragment.createDataMessageNode(userId, yourId);
        if (mCallBack != null && nodeMessage != null) {
            databaseReference.child(Common.MESSAGE_NODE)
                    .child(nodeMessage)
                    .removeValue()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mCallBack.onDeleteAllMessageSuccess();
                        } else {
                            mCallBack.onFailed("Error connect");
                        }
                    });
        }
    }

    public void unFriend(DatabaseReference databaseReference, String yourId, String userId) {
        if (mCallBack != null) {
            databaseReference.child(Common.FRIENDS_NODE)
                    .child(yourId)
                    .child(userId)
                    .removeValue()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            databaseReference.child(Common.FRIENDS_NODE)
                                    .child(userId)
                                    .child(yourId)
                                    .removeValue()
                                    .addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            mCallBack.onUnFriendSuccess(userId);
                                        } else {
                                            mCallBack.onFailed("Error connect");
                                        }
                                    });
                        } else {
                            mCallBack.onFailed("Error connect");
                        }
                    });
        }
    }
}