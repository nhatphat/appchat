package com.nathpath.rxjavarealmvv.presenter.content;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.callback.InfoUserFragmentView;
import com.nathpath.rxjavarealmvv.view.fragment.content.ConversationFragment;

import java.text.DateFormat;
import java.util.Date;

public class InfoUserFragmentPresenter {
    private final String ERROR = "Error connect";
    private InfoUserFragmentView mCallBack;

    public InfoUserFragmentPresenter(InfoUserFragmentView mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void getInfoUser(DatabaseReference databaseReference, String userId) {
        if (mCallBack != null) {
            databaseReference.child(Common.USER_NODE)
                    .child(userId)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            User user = dataSnapshot.getValue(User.class);
                            if(user != null){
                                mCallBack.onGetInfoUserSuccess(user);
                            }else {
                                Log.e("error", "get info user null");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }

    public void confirmRelative(DatabaseReference databaseReference, AuthManager authManager, String user_id) {
        if (mCallBack != null && authManager.infoUserAvailable()) {
            String mCurrentUid = authManager.getU_Id();
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (mCurrentUid.equals(user_id)) {
                        mCallBack.onConfirmRelativeSuccess(1);//yourself
                    } else if (dataSnapshot.child(Common.FRIENDS_NODE).child(mCurrentUid).hasChild(user_id)) {
                        mCallBack.onConfirmRelativeSuccess(2);//is friend
                    } else if (dataSnapshot.child(Common.REQUEST_NODE).child(mCurrentUid).hasChild(user_id)) {
                        mCallBack.onConfirmRelativeSuccess(3);//request
                    } else if (dataSnapshot.child(Common.REQUEST_NODE).child(user_id).hasChild(mCurrentUid)) {
                        mCallBack.onConfirmRelativeSuccess(4);//sent request
                    } else {
                        mCallBack.onConfirmRelativeSuccess(5);// no relative
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    mCallBack.onFailed(databaseError.getMessage());
                }
            });
        } else {
            if (mCallBack != null)
                mCallBack.onFailed("not realize relative");
            Log.e("error", "onDataChange confirm relative: current user id null");
        }
    }

    public void addFriend(DatabaseReference databaseReference, AuthManager authManager, String user_id) {
        if (mCallBack != null) {
            databaseReference.child(Common.REQUEST_NODE)
                    .child(user_id)
                    .child(authManager.getU_Id())
                    .setValue(DateFormat.getDateInstance().format(new Date()))
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mCallBack.onAddFriendSuccess();
                        } else {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }

    public void addFriendToFriendList(DatabaseReference databaseReference, AuthManager authManager, String user_id) {
        if (mCallBack != null) {
            databaseReference.child(Common.FRIENDS_NODE)
                    .child(authManager.getU_Id())
                    .child(user_id)
                    .setValue(DateFormat.getDateTimeInstance().format(new Date()))
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            databaseReference.child(Common.FRIENDS_NODE)
                                    .child(user_id)
                                    .child(authManager.getU_Id())
                                    .setValue(DateFormat.getDateTimeInstance().format(new Date()))
                                    .addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            mCallBack.onAddFriendToFriendListSuccess();
                                        } else {
                                            mCallBack.onFailed(ERROR);
                                        }
                                    });
                        } else {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }

    public void deleteFriendFromFriendList(DatabaseReference databaseReference, AuthManager authManager, String user_id) {
        if (mCallBack != null) {
            String currentUid = authManager.getU_Id();
            databaseReference.child(Common.FRIENDS_NODE)
                    .child(currentUid)
                    .child(user_id)
                    .removeValue()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            databaseReference.child(Common.FRIENDS_NODE)
                                    .child(user_id)
                                    .child(currentUid)
                                    .removeValue()
                                    .addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            String nodeMessage = ConversationFragment.createDataMessageNode(user_id, currentUid);
                                            if (mCallBack != null && nodeMessage != null) {
                                                databaseReference.child(Common.MESSAGE_NODE)
                                                        .child(nodeMessage)
                                                        .removeValue()
                                                        .addOnCompleteListener(task2 -> {
                                                            if (task2.isSuccessful()) {
                                                                mCallBack.onDeleteFriendToFriendListSuccess();
                                                            } else {
                                                                mCallBack.onFailed("Error connect");
                                                            }
                                                        });
                                            }
                                        } else {
                                            mCallBack.onFailed(ERROR);
                                        }
                                    });
                        } else {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }

    public void cancelRequest(DatabaseReference databaseReference, AuthManager authManager, String user_id, boolean becomeFriend) {
        if (mCallBack != null) {
            databaseReference.child(Common.REQUEST_NODE)
                    .child(authManager.getU_Id())
                    .child(user_id)
                    .removeValue()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mCallBack.onCancelRequestSuccess(becomeFriend);
                        } else {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }

    public void cancelSentRequest(DatabaseReference databaseReference, AuthManager authManager, String user_id) {
        if (mCallBack != null) {
            databaseReference.child(Common.REQUEST_NODE)
                    .child(user_id)
                    .child(authManager.getU_Id())
                    .removeValue()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mCallBack.onCancelSentRequestSuccess();
                        } else {
                            mCallBack.onFailed(ERROR);
                        }
                    });
        }
    }
}
