package com.nathpath.rxjavarealmvv.presenter.login;

import android.content.Context;
import android.view.animation.AnimationUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.callback.LoginActivityView;

public class LoginActivityPresenter {
    private Context context;
    private LoginActivityView mCallBack;
    private DatabaseReference mDataBase;
    private FirebaseAuth firebaseAuth;
    private AuthManager authManager;

    public LoginActivityPresenter(
            Context context, AuthManager authManager, DatabaseReference databaseReference, LoginActivityView callBack) {
        this.mCallBack = callBack;
        this.context = context;
        this.authManager = authManager;
        this.mDataBase = databaseReference.child(Common.USER_NODE);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void login(String username, String password) {
        firebaseAuth.signInWithEmailAndPassword(username, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FirebaseUser user = task.getResult().getUser();
                authManager.saveInfoCurrentUser(user);
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                mDataBase.child(user.getUid()).child(Common.TOKEN_USER)
                                        .setValue(task1.getResult().getToken());
                            }
                        });
                mCallBack.signInSuccess();
            } else {
                mCallBack.signInFailed(context.getString(R.string.email_or_password_incorrect));
            }
        });
    }

    public void register(String displayName, String username, String password) {
        firebaseAuth.createUserWithEmailAndPassword(username, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String u_id = task.getResult().getUser().getUid();
                User newUser = new User(
                        Common.AVATAR_DEFAULT,
                        Common.THUMB_AVATAR_DEFAULT,
                        displayName,
                        username,
                        Common.STATUS_DEFAULT
                );
                if (mDataBase != null) {
                    mDataBase.child(u_id).setValue(newUser).addOnCompleteListener(task1 -> {
                        if (task.isSuccessful()) {
                            mCallBack.registerSuccess();
                        }
                    });
                }
                return;
            }
            mCallBack.registerFailed(context.getString(R.string.existed_account));
        });
    }
}
