package com.nathpath.rxjavarealmvv.presenter.content;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bumptech.glide.request.ErrorRequestCoordinator;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nathpath.rxjavarealmvv.data.model.Message;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.callback.ConversationFragmentView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConversationFragmentPresenter {
    private ConversationFragmentView mCallBack;

    public ConversationFragmentPresenter(ConversationFragmentView mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void loadMessages(DatabaseReference databaseReference, String nodeDataMessage) {
        if (mCallBack != null) {
            List<Message> mData = new ArrayList<>();
            databaseReference.child(Common.MESSAGE_NODE)
                    .child(nodeDataMessage)
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                            if(dataSnapshot.exists()) {
                                Message newMessage = dataSnapshot.getValue(Message.class);
                                if (newMessage != null) {
                                    newMessage.setKey(dataSnapshot.getKey());
                                    if (newMessage.getFrom() != null) {
                                        mData.add(newMessage);
//                                        Collections.reverse(mData);//reverse
                                        mCallBack.onLoadMessageSuccess(mData);
//                                        Collections.reverse(mData);//reverse again
                                    } else {
                                        Log.e("error", "onChildAdded: data newMessage null");
                                    }
                                } else {
                                    Log.e("error", "onChildAdded: newMessage null");
                                }
                            }else{
                                mData.clear();
                                mCallBack.onLoadMessageSuccess(mData);
                            }
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            mCallBack.onFailed(databaseError.getMessage());
                        }
                    });
        }
    }

    public void deleteMessage(DatabaseReference databaseReference, String nodeMessage, String messageKey) {
        if (mCallBack != null) {
            databaseReference.child(Common.MESSAGE_NODE)
                    .child(nodeMessage)
                    .child(messageKey)
                    .removeValue()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mCallBack.onDeleteMessageSuccess();
                        } else {
                            mCallBack.onFailed("Không thể xóa tin nhắn");
                        }
                    });
        }
    }

    public void deleteImageMessage(String messageBody) {
        if (mCallBack != null) {
            FirebaseStorage.getInstance()
                    .getReferenceFromUrl(messageBody)
                    .delete()
                    .addOnSuccessListener(aVoid -> mCallBack.onDeleteImageMessageSuccess())
                    .addOnFailureListener((error) -> mCallBack.onFailed(error.getMessage()));
        }
    }

    public void loadInfoPartner(DatabaseReference databaseReference, String partnerId) {
        if (mCallBack != null) {
            databaseReference.child(Common.USER_NODE)
                    .child(partnerId)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            User object = dataSnapshot.getValue(User.class);
                            if (object != null) {
                                mCallBack.onLoadInfoPartnerSuccess(
                                        object.getDisplay_name(),
                                        object.getThumb_avatar()
                                );
                            } else {
                                mCallBack.onFailed("load info partner failed");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            mCallBack.onFailed(databaseError.getMessage());
                        }
                    });
        }
    }

    public void checkStatePartner(DatabaseReference databaseReference, String nodeMessage, String partnerId) {
        if (mCallBack != null) {
            databaseReference.child(Common.MESSAGE_NODE)
                    .child(Common.LAST_SEEN)
                    .child(nodeMessage)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.e("error", "last seen: " + dataSnapshot.child(partnerId).getValue());
                            Object object = dataSnapshot.child(partnerId).getValue();
                            if (object != null) {
                                long lastSeen = (long) object;
                                mCallBack.onCheckStatePartnerSuccess(lastSeen);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            mCallBack.onFailed(databaseError.getMessage());
                        }
                    });
        }
    }

    public void sendMessage(DatabaseReference databaseReference, String nodeDataMessage, Message newMessage) {
        if (mCallBack != null) {
            databaseReference.child(Common.MESSAGE_NODE)
                    .child(nodeDataMessage)
                    .push()
                    .setValue(newMessage)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mCallBack.onSendMessageSuccess();
                        } else {
                            mCallBack.onFailed("không thể gửi tin nhắn");
                        }
                    });
        }
    }

    public void sendImageMessage(DatabaseReference databaseReference, String nodeDataMessage, Uri image, Message message) {
        if (mCallBack != null) {
            String file_name = String.valueOf(System.currentTimeMillis()) + ".png";
            StorageReference image_path = FirebaseStorage.getInstance().getReference()
                    .child(Common.IMAGE_MESSAGE)
                    .child(nodeDataMessage)
                    .child(file_name);
            image_path.putFile(image).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    image_path.getDownloadUrl().addOnSuccessListener(uri -> {
                        String url_image = uri.toString();
                        message.setMessage(url_image);
                        sendMessage(databaseReference, nodeDataMessage, message);
                    });
                } else {
                    mCallBack.onFailed("không thể gửi tin nhắn");
                }
            });
        }
    }

    public void setTimeUserEnterAndExitConversation(DatabaseReference databaseReference, String nodeMessage, String userId) {
        databaseReference.child(Common.MESSAGE_NODE)
                .child(Common.LAST_SEEN)
                .child(nodeMessage)
                .child(userId)
                .setValue(ServerValue.TIMESTAMP);
    }
}
