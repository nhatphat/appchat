package com.nathpath.rxjavarealmvv.utils;

import com.nathpath.rxjavarealmvv.data.remote.APIServices;
import com.nathpath.rxjavarealmvv.data.remote.RetrofitClient;

public final class ApiUtils {
    public static final String BASE_URL = "https://api.myjson.com/";

    public static APIServices getApiService(){
        return RetrofitClient.getClient(BASE_URL).create(APIServices.class);
    }
}
