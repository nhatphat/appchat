package com.nathpath.rxjavarealmvv.utils;

public final class Common {

    //========================================================
    //                    database firebase
    //========================================================

    //========== user node =============
    public static final String USER_NODE = "User";                  //==============================
    public static final String TOKEN_USER = "tokenId";              //
    public static final String STATE_USER = "state";                //
    public static final String AVATAR_USER = "avatar";              //
    public static final String THUMB_AVATAR_USER = "thumb_avatar";  //         DON'T RENAME
    public static final String DISPLAY_NAME_USER = "display_name";  //
    public static final String EMAIL_USER = "email";                //
    public static final String STATUS_USER = "status";              //
    public static final String STATUS_DEFAULT = "Hi, everybody";    //==============================
    public static final String AVATAR_DEFAULT =
            "https://firebasestorage.googleapis.com/v0/b/mfirebase-myd.appspot.com/o/"
                    + "default%2Fic_app.png?alt=media&token=4164db27-aadc-4fc8-b688-a3224e492546";
    public static final String THUMB_AVATAR_DEFAULT =
            "https://firebasestorage.googleapis.com/v0/b/mfirebase-myd.appspot.com/o/"
                    + "default%2Fthumb_default.png?alt=media&token=d22f8643-e883-4c96-a998-c171ba370d0d";

    //========== request node ==============
    public static final String REQUEST_NODE = "Request";

    //========== friend node ===============
    public static final String FRIENDS_NODE = "Friend";

    //========== message node ==============
    public static final String MESSAGE_NODE = "Message";
    public static final String FROM_ID = "from";
    public static final String MESSAGE_CONTENT = "message";
    public static final String MESSAGE_TIME = "time";
    public static final String LAST_SEEN = "Last Seen";

    //==========================================================
    //                  storage firebase
    //==========================================================
    public static final String PROFILE_IMAGE = "Profile Image";
    public static final String AVATAR_PNG = "avatar.png";
    public static final String THUMB_AVATAR_PNG = "thumb_avatar.png";
    public static final String IMAGE_MESSAGE = "Image Message";

    public static final String BASE_IMAGE_URL = "https://firebasestorage.googleapis.com/v0/b/mfirebase-myd.appspot.com/o/";
}
