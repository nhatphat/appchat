package com.nathpath.rxjavarealmvv.utils;

import android.content.Context;

import com.nathpath.rxjavarealmvv.R;

public final class TimeAgo {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    public static String getTimeAgo(long time, Context context) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return context.getString(R.string.just_now);
        } /*else if (diff < 2 * MINUTE_MILLIS) {
            return ;
        } */ else if (diff < 50 * MINUTE_MILLIS) {
            return formatTimeAgo(diff / MINUTE_MILLIS, R.string.about_minutes_ago, context);
        } /*else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } */ else if (diff < 24 * HOUR_MILLIS) {
            return formatTimeAgo(diff / HOUR_MILLIS, R.string.about_hours_ago, context);
        } /*else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } */ else {
            return formatTimeAgo(diff / DAY_MILLIS, R.string.about_days_ago, context);
        }
    }

    private static String formatTimeAgo(long time, int stringID, Context context) {
        return context.getString(stringID, time);
    }
}
