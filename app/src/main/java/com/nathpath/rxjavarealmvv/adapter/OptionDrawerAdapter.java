package com.nathpath.rxjavarealmvv.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.data.model.OptionDrawer;

import java.util.List;

public class OptionDrawerAdapter extends RecyclerView.Adapter<OptionDrawerAdapter.ViewHolder> {
    private View mRootView;
    private List<OptionDrawer> mData;
    private int oldSelected = 0;
    private LinearLayout oldBackground;

    public OptionDrawerAdapter(List<OptionDrawer> mData) {
        this.mData = mData;
        mData.get(0).setSelected(true);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mRootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_option_drawer, parent, false);
        return new ViewHolder(mRootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.imgIcon.setImageResource(mData.get(position).getIcon());
        holder.tvTitle.setText(mData.get(position).getTitle());
        if(mData.get(position).getSelected()){
            holder.bgOption.setBackgroundResource(R.color.gray);
            oldBackground = holder.bgOption;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout bgOption;
        ImageView imgIcon;
        TextView tvTitle;

        private ViewHolder(final View itemView) {
            super(itemView);

            bgOption = itemView.findViewById(R.id.bgOption);
            imgIcon = itemView.findViewById(R.id.icon_option);
            tvTitle = itemView.findViewById(R.id.title_option);

            itemView.setOnClickListener(v -> {
                if (getLayoutPosition() != oldSelected) {
                    oldSelected = getLayoutPosition();
                    mData.get(getLayoutPosition()).setSelected(true);
                    mData.get(oldSelected).setSelected(false);
                    oldBackground.setBackgroundResource(R.color.white);
                    bgOption.setBackgroundResource(R.color.gray);
                    oldBackground = bgOption;
                    if (onOptionClickListener != null) {
                        onOptionClickListener.OnOptionClick(itemView, getLayoutPosition());
                    }
                }
            });
        }
    }

    //===============listener===================

    public interface onOptionClickListener {
        void OnOptionClick(View v, int position);
    }

    private static onOptionClickListener onOptionClickListener;

    public void setOnOptionClickListner(onOptionClickListener listener) {
        onOptionClickListener = listener;
    }
}
