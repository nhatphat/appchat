package com.nathpath.rxjavarealmvv.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.data.model.UserViewHolder;
import java.util.List;

public class FriendAdapter extends RecyclerView.Adapter<UserViewHolder>{
    private List<User> mData;

    public FriendAdapter(List<User> list){
        this.mData = list;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User model = mData.get(position);
        holder.tvUserDislayName.setText(model.getDisplay_name());
        holder.tvUserStatus.setText(model.getStatus());
        loadImageInto(model.getThumb_avatar(), holder.civUserAvatar);
        loadImageInto(model.getState() ? R.drawable.ic_online : R.drawable.ic_offline, holder.imgUserState);

        holder.currentView.setOnClickListener(v -> {
            if(onItemClickListener != null){
                onItemClickListener.onItemClick(holder.currentView, position);
            }
        });

        holder.currentView.setOnLongClickListener(v -> {
            if(onItemClickListener != null){
                onItemClickListener.onItemLongClick(holder.user, position);
                return true;
            }
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    //===== load image from url===========
    private void loadImageInto(String imageURL, ImageView imageView) {
        Glide.with(imageView.getContext())
                .load(imageURL)
                .into(imageView);
    }

    //===== load image from resource=======
    private void loadImageInto(int imageID, ImageView imageView) {
        Glide.with(imageView.getContext())
                .load(imageID)
                .into(imageView);
    }

    //=======listener======
    public interface OnItemClickListener{
        void onItemClick(View view, int position);
        void onItemLongClick(View view, int position);
    }

    private static OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }
}
