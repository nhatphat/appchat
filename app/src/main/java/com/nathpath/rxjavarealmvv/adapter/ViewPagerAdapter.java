package com.nathpath.rxjavarealmvv.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.PageContent;
import com.nathpath.rxjavarealmvv.utils.Common;

import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private List<PageContent> list;

    public ViewPagerAdapter(Context context, FragmentManager fm, List<PageContent> list) {
        super(fm);
        this.context = context;
        this.list = list;
    }

    @Override
    public BaseFragment getItem(int position) {
        return list.get(position).getFragmentPage();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @SuppressLint("InflateParams")
    public View getTabView(int position, boolean isSelected) {
        if (context != null) {
            View v;
            ImageView imgIcon;
            TextView tvTitle;

            v = LayoutInflater.from(context).inflate(R.layout.tablayout_custom, null);
            imgIcon = v.findViewById(R.id.imgIcon_tablayout);
            tvTitle = v.findViewById(R.id.tvTitle_tablayout);
            imgIcon.setImageResource(list.get(position).getIconPageSelect());

            if (!isSelected) {
                tvTitle.setVisibility(View.GONE);
//                imgIcon.setImageResource(list.get(position).getIconPageNone());
                return v;
            } else {
//                imgIcon.setImageResource(list.get(position).getIconPageSelect());
                tvTitle.setText(list.get(position).getTitlePage());
                tvTitle.setVisibility(View.VISIBLE);
                return v;
            }
        }
        return null;
    }
}
