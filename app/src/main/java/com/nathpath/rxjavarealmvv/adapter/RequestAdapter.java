package com.nathpath.rxjavarealmvv.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.data.model.UserViewHolder;

import java.util.List;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.RequestViewHolder>{
    private List<User> data;

    public RequestAdapter(List<User> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new RequestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestViewHolder holder, int position) {
        User model = data.get(position);
        holder.tvUserDislayName.setText(model.getDisplay_name());
        holder.tvUserStatus.setText(model.getStatus());
        loadImageInto(model.getThumb_avatar(), holder.civUserAvatar);
        loadImageInto(model.getState() ? R.drawable.ic_online : R.drawable.ic_offline, holder.imgUserState);

        setOnClick(holder.user, position);
        setOnClick(holder.btnAccept, position);
        setOnClick(holder.btnCancelAccept, position);
    }

    private void setOnClick(View view, int position){
        view.setOnClickListener(v -> {
            if(onItemClickListener != null){
                onItemClickListener.onItemClick(view, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //===== load image from url===========
    private void loadImageInto(String imageURL, ImageView imageView) {
        Glide.with(imageView.getContext())
                .load(imageURL)
                .into(imageView);
    }

    //===== load image from resource=======
    private void loadImageInto(int imageID, ImageView imageView) {
        Glide.with(imageView.getContext())
                .load(imageID)
                .into(imageView);
    }

    //=======listener======
    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    private static OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public static class RequestViewHolder extends UserViewHolder {
        private RelativeLayout user;
        private RelativeLayout request;
        private Button btnAccept;
        private Button btnCancelAccept;

        public RequestViewHolder(View itemView) {
            super(itemView);

            user = itemView.findViewById(R.id.user);
            request = itemView.findViewById(R.id.forRequest);
            request.setVisibility(View.VISIBLE);
            btnAccept = itemView.findViewById(R.id.btnAccept);
            btnCancelAccept = itemView.findViewById(R.id.btnCancelAccept);
        }
    }
}
