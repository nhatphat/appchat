package com.nathpath.rxjavarealmvv.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.data.model.Message;
import com.nathpath.rxjavarealmvv.utils.Common;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private String oldDate = "";

    private List<Message> mData;
    private String partnerId;
    private DatabaseReference databaseReference;

    public MessageAdapter(List<Message> list, DatabaseReference databaseReference, String partnerId) {
        this.mData = list;
        this.databaseReference = databaseReference;
        this.partnerId = partnerId;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        return new MessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        Message message = mData.get(position);
        String from = message.getFrom();
        if (from == null) {
            //failed
            Log.e("error", "onBindViewHolder: message from null");
        } else if (from.equals(partnerId)) {
            switchPartnerMessage(message, holder);
        } else {
            switchYourMessage(message, holder);
        }

        setOnLongClick(holder.tvPartnerMessage, message, position);
        setOnLongClick(holder.tvYourMessage, message, position);
        setOnLongClick(holder.imgPartnerMessage, message, position);
        setOnLongClick(holder.imgYourMessage, message, position);
        setOnClick(holder.civPartnerAvatar, message, position);
        setOnClick(holder.imgPartnerMessage, message, position);
        setOnClick(holder.imgYourMessage, message, position);
    }



    private void setOnClick(View view, Message message, int position){
        view.setOnClickListener(v -> {
            if(onItemClickListener != null){
                onItemClickListener.onItemClick(view, message, position);
            }
        });
    }

    private void setOnLongClick(View view, Message message, int position){
        view.setOnLongClickListener(v -> {
            if(onItemClickListener != null){
                onItemClickListener.onItemLongClick(view, message, position);
                return true;
            }
            return false;
        });
    }

    private void switchPartnerMessage(Message message, MessageViewHolder holder) {
        holder.yourMessage.setVisibility(View.GONE);
        holder.partnerMessage.setVisibility(View.VISIBLE);
        databaseReference.child(Common.USER_NODE)
                .child(partnerId)
                .child(Common.THUMB_AVATAR_USER)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            loadImageInto(dataSnapshot.getValue().toString(), holder.civPartnerAvatar);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        String messages = message.getMessage();
        String time = setDateMessage(message.getTime(), holder.dateMessage);
        if (messages.contains(Common.BASE_IMAGE_URL)) {
            // is image
            holder.partnerMessageText.setVisibility(View.GONE);
            holder.partnerMessageImage.setVisibility(View.VISIBLE);
            loadImageInto(messages, holder.imgPartnerMessage);
            holder.tvPartnerMessageImageTime.setText(time);
        } else {
            // is text
            holder.partnerMessageText.setVisibility(View.VISIBLE);
            holder.partnerMessageImage.setVisibility(View.GONE);
            holder.tvPartnerMessage.setText(messages);
            holder.tvPartnerMessageTextTime.setText(time);
        }
    }

    private void switchYourMessage(Message message, MessageViewHolder holder) {
        holder.yourMessage.setVisibility(View.VISIBLE);
        holder.partnerMessage.setVisibility(View.GONE);
        String messages = message.getMessage();
        String time = setDateMessage(message.getTime(), holder.dateMessage);
        if (messages.contains(Common.BASE_IMAGE_URL)) {
            // is image
            holder.yourMessageText.setVisibility(View.GONE);
            holder.yourMessageImage.setVisibility(View.VISIBLE);
            loadImageInto(messages, holder.imgYourMessage);
            holder.tvYourMessageImageTime.setText(time);
        } else {
            // is text
            holder.yourMessageText.setVisibility(View.VISIBLE);
            holder.yourMessageImage.setVisibility(View.GONE);
            holder.tvYourMessage.setText(messages);
            holder.tvYourMessageTextTime.setText(time);
        }
    }

    private void switchDateMessage(TextView date, String time, boolean isShow) {
        if (isShow) {
            date.setText(time);
            date.setVisibility(View.VISIBLE);
        } else {
            date.setVisibility(View.GONE);
        }
    }

    private String setDateMessage(String time, TextView date) {// update later
        final String REGEX = " - ";
        String[] timeMessage = time.split(REGEX);
        String dateMessage = timeMessage[1];
        if (!oldDate.equals(dateMessage)) {
            oldDate = dateMessage;
            switchDateMessage(date, time, true);
        } else {
            switchDateMessage(date, time, false);
        }
        return timeMessage[0];
    }

    //===== load image from url===========
    private void loadImageInto(String imageURL, ImageView imageView) {
        Glide.with(imageView.getContext())
                .load(imageURL)
                .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private TextView dateMessage;

        //===partner message====
        private RelativeLayout partnerMessage;
        private CircleImageView civPartnerAvatar;
        private LinearLayout partnerMessageText;
        private TextView tvPartnerMessage;
        private TextView tvPartnerMessageTextTime;
        private RelativeLayout partnerMessageImage;
        private ImageView imgPartnerMessage;
        private TextView tvPartnerMessageImageTime;

        //=====your message====
        private RelativeLayout yourMessage;
        private LinearLayout yourMessageText;
        private TextView tvYourMessage;
        private TextView tvYourMessageTextTime;
        private RelativeLayout yourMessageImage;
        private ImageView imgYourMessage;
        private TextView tvYourMessageImageTime;


        public MessageViewHolder(View itemView) {
            super(itemView);

            dateMessage = itemView.findViewById(R.id.dateMessage);

            partnerMessage = itemView.findViewById(R.id.partnerMessage);
            civPartnerAvatar = itemView.findViewById(R.id.civPartnerAvatar);
            partnerMessageText = itemView.findViewById(R.id.partnerMessageText);
            tvPartnerMessage = itemView.findViewById(R.id.tvPartnerMessage);
            tvPartnerMessageTextTime = itemView.findViewById(R.id.tvPartnerMessageTextTime);
            partnerMessageImage = itemView.findViewById(R.id.partnerMessageImage);
            imgPartnerMessage = itemView.findViewById(R.id.imgPartnerMessage);
            tvPartnerMessageImageTime = itemView.findViewById(R.id.tvPartnerMessageImageTime);

            yourMessage = itemView.findViewById(R.id.yourMessage);
            yourMessageText = itemView.findViewById(R.id.yourMessageText);
            tvYourMessage = itemView.findViewById(R.id.tvYourMessage);
            tvYourMessageTextTime = itemView.findViewById(R.id.tvYourMessageTextTime);
            yourMessageImage = itemView.findViewById(R.id.yourMessageImage);
            imgYourMessage = itemView.findViewById(R.id.imgYourMessage);
            tvYourMessageImageTime = itemView.findViewById(R.id.tvYourMessageImageTime);
        }
    }

    //=======listener======
    public interface OnItemClickListener {
        void onItemClick(View view,Message message, int position);
        void onItemLongClick(View view, Message message, int position);
    }

    private static OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }
}
