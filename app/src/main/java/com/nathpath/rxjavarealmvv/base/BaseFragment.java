package com.nathpath.rxjavarealmvv.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nathpath.rxjavarealmvv.services.AuthManager;

public abstract class BaseFragment extends Fragment implements View.OnClickListener, BaseActivity.onBackPressedListener {
    protected BaseActivity mActivity;
    protected View mRootView;
    protected DatabaseReference databaseReference;
    protected AuthManager authManager;
    protected Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        databaseReference = GeneralActivity.databaseReference;
        authManager = mActivity.authManager;
        context = mActivity.context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutResource(), container, false);
        return mRootView;
    }

    protected abstract @LayoutRes
    int getLayoutResource();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initView(mRootView);
        setData();
        event();
    }

    protected abstract void initView(View rootView);

    protected abstract void setData();

    protected abstract void event();

    public String getFragmentName() {
        return this.getClass().getSimpleName();
    }

    protected void toast(String s) {
        Toast.makeText(mActivity, s, Toast.LENGTH_SHORT).show();
    }

//    public void addFragment(@IdRes int container, Fragment fragment) {
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        BaseFragment oldFragment = getCurrentFragmentOn(container);
//        if (oldFragment != null) {
//            transaction.replace(container, fragment);
//            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//            transaction.addToBackStack(oldFragment.getFragmentName());
//        } else {
//            transaction.add(container, fragment);
//        }
//        transaction.commit();
//    }

//    public BaseFragment getCurrentFragmentOn(@IdRes int container) {
//        return (BaseFragment) getChildFragmentManager().findFragmentById(container);
//    }

    protected void showLoading(boolean isShow) {
        if (mActivity != null) {
            mActivity.showLoading(isShow);
        }
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
