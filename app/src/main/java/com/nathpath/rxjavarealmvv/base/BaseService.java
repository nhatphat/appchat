package com.nathpath.rxjavarealmvv.base;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

public abstract class BaseService extends Service{

    protected BaseService mService;
    protected Binder mBinder = new LocalBinder();

    private BaseService getService(){
        if(mService == null){
            mService = this;
        }
        return mService;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    public class LocalBinder extends Binder{
        public BaseService getServices(){
            return getService();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        InitData();
    }

    protected void InitData(){

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        runAction();

        return START_NOT_STICKY;
    }

    protected void runAction(){

    }

    @Override
    public void onDestroy() {

        stopCurrentService();

        super.onDestroy();
    }

    protected void stopCurrentService(){

    }


}
