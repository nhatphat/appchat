package com.nathpath.rxjavarealmvv.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.services.NetworkState;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

public abstract class GeneralActivity extends AppCompatActivity {
    private static final boolean FABRIC_TRACKING = false;
    private static boolean REALM_AND_FIREBASE_PERSISTENCE = false;
    public static DatabaseReference databaseReference;

    public Context context;
    public NetworkState receiver;
    public AuthManager authManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (FABRIC_TRACKING) {
            Fabric.with(this, new Crashlytics());
        }

        context = getApplicationContext();

        if (!REALM_AND_FIREBASE_PERSISTENCE) {
//            Realm.init(context);
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            REALM_AND_FIREBASE_PERSISTENCE = true;
        }

        receiver = NetworkState.getInstance();
        if (databaseReference == null) {
            databaseReference = FirebaseDatabase.getInstance().getReference();
            databaseReference.keepSynced(true);
        }
        authManager = AuthManager.getInstance(context, databaseReference);

    }

//    private void registerReceiver(){
//
//        if(!receiver.isRegister){
//            receiver.registerReceiver(this);
//        }
//    }

    public <T extends AppCompatActivity> void switchActivity(Class<T> activity, Bundle data) {
        Intent toActivity = new Intent(this, activity);
        if (data != null) {
            toActivity.putExtras(data);
        }
        startActivity(toActivity);
        ActivityCompat.finishAffinity(this);
    }
}
