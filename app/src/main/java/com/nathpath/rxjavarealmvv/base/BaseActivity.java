package com.nathpath.rxjavarealmvv.base;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.services.NetworkState;
import com.nathpath.rxjavarealmvv.view.activity.lost_network.LostNetworkActivity;
import com.nathpath.rxjavarealmvv.view.fragment.dialog.Loading;

import java.util.Objects;

public abstract class BaseActivity extends GeneralActivity
        implements Loading.onCancelListener, NetworkState.NetworkStateListener {
    public static final String ACTIVITY_LOST_NETWORK = "activity_lost_network";

    private Loading dialogLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        dialogLoading = new Loading();

        OnCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetWork();
    }

    protected void checkNetWork() {
        if (!NetworkState.isNetworkAvailable(this)) {
            onLost();
        }
    }

    protected abstract @LayoutRes
    int getLayoutResource();

    protected abstract void OnCreate();

    @Override
    protected void onDestroy() {
        //receiver.unRegisterReceiver();
        OnDestroy();
        super.onDestroy();
    }

    protected abstract void OnDestroy();

    protected void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    protected void addFragment(@IdRes int container, BaseFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        BaseFragment oldFragment = getCurrentFragmentOn(container);
        if (oldFragment != null) {
            transaction.replace(container, fragment);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.addToBackStack(oldFragment.getFragmentName());
        } else {
            transaction.add(container, fragment);
        }
        transaction.commit();
    }

    protected BaseFragment getCurrentFragmentOn(@IdRes int container) {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(container);
    }

    public void showLoading(boolean isShow) {
        if (isShow && !dialogLoading.isAdded()) {
            dialogLoading.show(getSupportFragmentManager(), dialogLoading.getDialogFragmentName());
        } else {
            if (!getSupportFragmentManager().isStateSaved() && dialogLoading.getShowsDialog()) {
                dialogLoading.dismiss();
            }
        }
    }

    protected void showSnackBar(View container, String textSnackBar) {
        final Snackbar snackbar = Snackbar.make(container, textSnackBar, Snackbar.LENGTH_LONG);
        snackbar.show();
        snackbar.setAction(R.string.hide, (v) -> snackbar.dismiss());
    }

    protected void switchLostNetwork() {
        Bundle data = new Bundle();
        data.putString(ACTIVITY_LOST_NETWORK, this.getClass().getCanonicalName());
        switchActivity(LostNetworkActivity.class, data);
    }

    public void hideSoftInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null){
            imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
        }
    }

//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
////        hideSoftInput();
//        return super.dispatchTouchEvent(ev);
//    }

    public interface onBackPressedListener {
        boolean onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //======network state======

    @Override
    public void onAvailable() {
        authManager.setUserState(true);
        toast(getString(R.string.connected));
    }
}
