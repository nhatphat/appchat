package com.nathpath.rxjavarealmvv.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseDialogFragment extends DialogFragment implements View.OnClickListener{
    private Dialog mDialog;
    protected AppCompatActivity mActivity;
    protected View rootView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof AppCompatActivity){
            mActivity = (AppCompatActivity) context;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mDialog = setDialog();
        return mDialog;
    }

    protected abstract Dialog setDialog();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutResource(), container, false);

        initView(rootView);
        return rootView;
    }

    protected abstract @LayoutRes int getLayoutResource();

    protected abstract void initView(View rootView);

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setData();
        event();
    }

    protected abstract void setData();

    protected abstract void event();

    public String getDialogFragmentName(){
        return this.getClass().getSimpleName();
    }
}
