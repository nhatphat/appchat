package com.nathpath.rxjavarealmvv.view.callback;

import com.nathpath.rxjavarealmvv.data.model.Message;

import java.util.List;

public interface ConversationFragmentView {
    void onLoadMessageSuccess(List<Message> list);

    void onDeleteMessageSuccess();

    void onDeleteImageMessageSuccess();

    void onLoadInfoPartnerSuccess(String name, String avatar);

    void onCheckStatePartnerSuccess(long lastSeen);

    void onSendMessageSuccess();

    void onFailed(String err);
}
