package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.app.AlertDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.adapter.FriendAdapter;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.presenter.content.FriendFragmentPresenter;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.callback.FriendFragmentView;
import com.nathpath.rxjavarealmvv.view.fragment.dialog.BottomOptionFriendFragment;

import java.util.ArrayList;
import java.util.List;

public class FriendFragment extends BaseFragment implements FriendFragmentView, BottomOptionFriendFragment.OnOptionClickListener {
    private FloatingActionButton fabMessage;

    private List<String> idFriendList;
    private List<User> mData;
    private FriendAdapter mAdapter;
    private RecyclerView rvMessage;
    private TextView tvNoFriend;
    private RelativeLayout userSelected;
    private BottomOptionFriendFragment bottomOptionFriendFragment;

    private FriendFragmentPresenter mPresenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_friend;
    }

    @Override
    protected void initView(View rootView) {
        fabMessage = rootView.findViewById(R.id.fabMessage);
        rvMessage = rootView.findViewById(R.id.rvMessage);
        tvNoFriend = rootView.findViewById(R.id.noFriend);
    }

    @Override
    protected void setData() {
        mPresenter = new FriendFragmentPresenter(this);

        idFriendList = new ArrayList<>();

        mData = new ArrayList<>();
        mAdapter = new FriendAdapter(mData);

        rvMessage.setHasFixedSize(true);
        rvMessage.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        rvMessage.setAdapter(mAdapter);

        mPresenter.loadFriendList(databaseReference, authManager);
    }

    @Override
    protected void event() {
        fabMessage.setOnClickListener(this);
        mAdapter.setOnItemClickListener(new FriendAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ((MainActivity) mActivity).addChildFragment(ConversationFragment.newInstance(idFriendList.get(position)));
            }

            @Override
            public void onItemLongClick(View view, int position) {
                if (view instanceof RelativeLayout) {
                    userSelected = (RelativeLayout) view;
                    userSelected.setBackgroundResource(R.color.light_gray);
                }
                showBottomOption(mData.get(position), idFriendList.get(position));
            }
        });
    }

    private void showBottomOption(User user, String userId) {
        bottomOptionFriendFragment = BottomOptionFriendFragment.newInstance(user, userId);
        bottomOptionFriendFragment.show(getChildFragmentManager(), "bottom_option");
        bottomOptionFriendFragment.setOnOptionClickListener(this);
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabMessage:
                ((MainActivity) mActivity).addChildFragment(new AllUserFragment());
                break;
        }
    }

    @Override
    public void onLoadFriendListSuccess(List<User> friendList, List<String> idList, boolean nulll) {
        if (nulll) {
            mData.clear();
            idFriendList.clear();
            mAdapter.notifyDataSetChanged();
            tvNoFriend.setVisibility(View.VISIBLE);
        } else {
            if (friendList != null && idList != null) {
                tvNoFriend.setVisibility(View.GONE);
                mData.clear();
                mData.addAll(friendList);
                mAdapter.notifyDataSetChanged();

                idFriendList.clear();
                idFriendList.addAll(idList);
            }
        }
    }

    @Override
    public void onDeleteAllMessageSuccess() {
        setUserSelectedNone();
        toast(getString(R.string.deleted_message));
        bottomOptionFriendFragment.dismiss();
    }

    @Override
    public void onUnFriendSuccess(String userId) {
        setUserSelectedNone();
        toast(getString(R.string.un_friend_ok));
        mPresenter.deleteAllMessage(databaseReference, authManager.getU_Id(), userId);
        bottomOptionFriendFragment.dismiss();
    }

    @Override
    public void onFailed(String err) {
        toast(err);
    }

    private void setUserSelectedNone(){
        if (userSelected != null) {
            userSelected.setBackgroundResource(R.color.white);
        }
    }

    @Override
    public void onOptionClick(View view, String userID) {
        switch (view.getId()) {
            case R.id.optionExit:
                setUserSelectedNone();
                break;

            case R.id.optionDeleteMessage:
                confirmOption(getString(R.string.delete_all_message), userID, 0);
                break;

            case R.id.optionUnFriend:
                confirmOption(getString(R.string.un_friend), userID, 1);
                break;
        }
    }

    //===========================================
    //             Action
    //    0: delete message
    //    1: un friend
    //
    //===========================================
    private void confirmOption(String title, String userId, int action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setIcon(R.drawable.ic_warning)
                .setTitle(title)
                .setMessage(getString(R.string.confirm_cancel, title))
                .setNegativeButton(R.string.no, (dialog, which) -> {
                    //
                })
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    switch (action) {
                        case 0:
                            mPresenter.deleteAllMessage(databaseReference, authManager.getU_Id(), userId);
                            break;
                        case 1:
                            mPresenter.unFriend(databaseReference, authManager.getU_Id(), userId);
                            break;
                    }
                }).show();
    }
}
