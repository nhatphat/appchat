package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.Manifest;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.adapter.ContactAdapter;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.Contact;
import com.nathpath.rxjavarealmvv.presenter.content.ContactFragmentPresenter;
import com.nathpath.rxjavarealmvv.view.callback.ContactFragmentView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ContactFragment extends BaseFragment implements ContactFragmentView {
    private RecyclerView listContact;
    private static List<Contact> mData;
    private static ContactAdapter mAdapter;
    private SwipeRefreshLayout srlRefresh;
    private TextView tvSwipeToRefresh;
    private final int INCR = 1;
    private final int DESC = -1;
    private final int PERMISSIONS_REQUEST_READ_CONTACTS = 555;

    private ContactFragmentPresenter presenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_contact;
    }

    @Override
    protected void initView(View rootView) {
        listContact = rootView.findViewById(R.id.list_contact);
        srlRefresh = rootView.findViewById(R.id.srlRefresh);
        tvSwipeToRefresh = rootView.findViewById(R.id.swipeToRefresh);
    }

    @Override
    protected void setData() {
        tvSwipeToRefresh.setVisibility(View.GONE);
        if (!checkPermissionContact()) {
            tvSwipeToRefresh.setVisibility(View.VISIBLE);
            permissionRequest();
        }

        presenter = new ContactFragmentPresenter(context, this);

        listContact.setHasFixedSize(true);
        listContact.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));

        if (mData == null) {
            mData = new ArrayList<>();
            mAdapter = new ContactAdapter(mData);
        }
        listContact.setAdapter(mAdapter);
        if(checkPermissionContact()) {
            presenter.loadContact();
        }
    }

    public void permissionRequest() {
        ActivityCompat.requestPermissions(mActivity,
                new String[]{Manifest.permission.READ_CONTACTS},
                PERMISSIONS_REQUEST_READ_CONTACTS);
    }

    private boolean checkPermissionContact() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void event() {
        mAdapter.setOnItemClickListener((v, position) -> {
            toast(mData.get(position).getName());
        });

        srlRefresh.setOnRefreshListener(() -> {
            tvSwipeToRefresh.setVisibility(View.GONE);
            srlRefresh.setRefreshing(true);
            presenter.loadContact();
        });
    }

    @Override
    public void onClick(View v) {

    }

    private void sortData(int sort){
        if(sort == 1){
            Collections.sort(mData);
        }else{
            Collections.sort(mData, Collections.reverseOrder());
        }
    }

    //========load contact=======
    @Override
    public void loadContactSuccess(List<Contact> list) {
        mData.clear();
        mData.addAll(list);
        sortData(INCR);
        mAdapter.notifyDataSetChanged();
        srlRefresh.setRefreshing(false);
    }

    @Override
    public void loadContactFailed(String err) {
        srlRefresh.setRefreshing(false);
        tvSwipeToRefresh.setVisibility(View.VISIBLE);
    }
}
