package com.nathpath.rxjavarealmvv.view.fragment.option;

import android.view.View;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.BuildConfig;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;

public class AboutFragment extends BaseFragment {
    public final String version = "Version: "
            + BuildConfig.VERSION_NAME
            + "."
            + BuildConfig.VERSION_CODE;

    private TextView tvVersion;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_about;
    }

    @Override
    protected void initView(View rootView) {
        tvVersion = rootView.findViewById(R.id.version);
    }

    @Override
    protected void setData() {
        tvVersion.setText(version);
    }

    @Override
    protected void event() {

    }

    @Override
    public void onClick(View v) {

    }
}
