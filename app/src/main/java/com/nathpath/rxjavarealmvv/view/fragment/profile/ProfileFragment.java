package com.nathpath.rxjavarealmvv.view.fragment.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.presenter.profile.ProfileFragmentPresenter;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.callback.ProfileFragmentView;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment implements ProfileFragmentView {
    private RelativeLayout profileStateChange;
    private ImageView imgExitProfile;
    private ImageView imgEditAvatar;
    private ImageView imgEditDisplayName;
    private CircleImageView imgProfileAvatar;
    private EditText edtDisplayName;
    private EditText edtEmail;
    private EditText edtStatus;
    private Button btnEditStatus;
    private Button btnCancelUpdate;
    private Button btnUpdate;

    private static final int REQUEST_CODE_IMAGE = 113;
    private static final int TAKE_PICTURE = 1;
    private Uri avatar;
    private Uri takePic;

    private ProfileFragmentPresenter presenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void initView(View rootView) {
        imgExitProfile = rootView.findViewById(R.id.imgExitProfile);
        imgEditAvatar = rootView.findViewById(R.id.imgEditAvatar);
        btnEditStatus = rootView.findViewById(R.id.btnEditStatus);
        imgEditDisplayName = rootView.findViewById(R.id.imgEditDisplayName);
        imgProfileAvatar = rootView.findViewById(R.id.imgProfileAvatar);
        edtDisplayName = rootView.findViewById(R.id.profileDisplayName);
        edtEmail = rootView.findViewById(R.id.profileEmail);
        edtStatus = rootView.findViewById(R.id.edtStatus);
        profileStateChange = rootView.findViewById(R.id.profile_state_change);
        btnCancelUpdate = rootView.findViewById(R.id.btnCancelUpdateProfile);
        btnUpdate = rootView.findViewById(R.id.btnUpdateProfile);
    }

    @Override
    protected void setData() {
        noEdit();
        presenter = new ProfileFragmentPresenter(context, authManager, this);

        setInfoCurrentUser();
    }

    private void setInfoCurrentUser() {
        if (authManager.infoUserAvailable()) {
            loadImageInto(authManager.getUrlAvatar(), imgProfileAvatar);
            edtDisplayName.setText(authManager.getDisplayName());
            edtStatus.setText(authManager.getStatus());
            edtEmail.setText(authManager.getEmail());
        }
    }

    @Override
    protected void event() {
        imgExitProfile.setOnClickListener(this);
        imgEditAvatar.setOnClickListener(this);
        imgEditDisplayName.setOnClickListener(this);

        btnEditStatus.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        btnCancelUpdate.setOnClickListener(this);
    }

    private void takePhoto() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, TAKE_PICTURE);
    }

    private void chossePic() {
        Intent chossePic = new Intent();
        chossePic.setType("image/*");
        chossePic.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(chossePic, "CHOOSE_PIC"), REQUEST_CODE_IMAGE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgExitProfile:
                mActivity.onBackPressed();
                break;

            case R.id.imgEditAvatar:
                cropImage();
                break;

            case R.id.btnEditStatus:
                edtStatus.setEnabled(true);
                edit();
                break;

            case R.id.imgEditDisplayName:
                edtDisplayName.setEnabled(true);
                edit();
                break;

            case R.id.btnUpdateProfile:
                String displayName = edtDisplayName.getText().toString();
                String status = edtStatus.getText().toString();
                if (displayName.equals("")) {
                    edtDisplayName.setError(getString(R.string.not_blank));
                } else {
                    showLoading(true);
                    presenter.updateInfoCurrentUser(displayName, status, avatar);
                }
                break;

            case R.id.btnCancelUpdateProfile:
                edtDisplayName.setText(authManager.getDisplayName());
                loadImageInto(authManager.getUrlAvatar(), imgProfileAvatar);
                noEdit();
                break;
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == REQUEST_CODE_IMAGE && resultCode == RESULT_OK) {
//            Uri image = data.getData();
//            cropImage();
//        }
//
//        if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
//            Uri image = data.getData();
//            cropImage();
//        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                avatar = result.getUri();
                edit();
                loadImageInto(avatar, imgProfileAvatar);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                avatar = null;
            }
        }
    }

    private void cropImage(){
        CropImage.activity()
                .setAspectRatio(1, 1)
                .start(mActivity.context, ProfileFragment.this);
    }

    //===== load image from url===========
    private void loadImageInto(String imageURL, ImageView imageView) {
        Glide.with(this)
                .load(imageURL)
                .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                .into(imageView);
    }

    //===== load image from uri=======
    protected void loadImageInto(Uri imageURI, ImageView imageView) {
        Glide.with(this)
                .load(imageURI)
                .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                .into(imageView);
    }

    private void edit() {
        profileStateChange.setVisibility(View.VISIBLE);
    }

    private void noEdit() {
        avatar = null;
        edtDisplayName.setEnabled(false);
        edtStatus.setEnabled(false);
        profileStateChange.setVisibility(View.GONE);
    }

    //======update info user=======
    @Override
    public void onUpdateSuccess(String s) {
        ((MainActivity) mActivity).setInfoCurrentUser();
        noEdit();
        showLoading(false);
        toast(s);
    }

    @Override
    public void onUpdateFailed(String e) {
        noEdit();
        showLoading(false);
        toast(e);
    }
}
