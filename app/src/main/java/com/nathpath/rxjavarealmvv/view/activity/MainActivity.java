package com.nathpath.rxjavarealmvv.view.activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.adapter.OptionDrawerAdapter;
import com.nathpath.rxjavarealmvv.base.BaseActivity;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.OptionDrawer;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.presenter.main.MainActivityPresenter;
import com.nathpath.rxjavarealmvv.services.NetworkState;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.activity.login.LoginActivity;
import com.nathpath.rxjavarealmvv.view.activity.preferences.PreferencesActivity;
import com.nathpath.rxjavarealmvv.view.callback.MainActivityView;
import com.nathpath.rxjavarealmvv.view.fragment.content.InfoUserFragment;
import com.nathpath.rxjavarealmvv.view.fragment.profile.ProfileFragment;
import com.nathpath.rxjavarealmvv.view.fragment.option.AboutFragment;
import com.nathpath.rxjavarealmvv.view.fragment.option.HelpFragment;
import com.nathpath.rxjavarealmvv.view.fragment.option.HomeFragment;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends BaseActivity implements MainActivityView {
    private DrawerLayout drawerLayout;
    private RelativeLayout mainContent;
    private FrameLayout containerFragment;
    //    private ImageView imgOpenLeftMenu;
    private ImageView imgPower;
    private RecyclerView recyclerView;
    private List<OptionDrawer> mOptions;
    private OptionDrawerAdapter mAdapter;
    private ActionBarDrawerToggle drawerToggle;
    private TextView tvPreference;
    private TextView tvDisplayName;
    private TextView tvEmail;
    private CircleImageView imgAvatar;

    private HomeFragment homeFragment;
    private AboutFragment aboutFragment;
    private HelpFragment helpFragment;

    private MainActivityPresenter presenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void OnCreate() {
        homeFragment = new HomeFragment();
        aboutFragment = new AboutFragment();
        helpFragment = new HelpFragment();

        addChildFragment(homeFragment);
        receiver.registerReceiver(this);

        initView();
        initData();
        event();
        setInfoCurrentUser();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (NetworkState.isNetworkAvailable(this)) {
            authManager.setUserState(true);
        }

//        if (authManager.infoUserAvailable()) {
//            String mCurrentUid = authManager.getU_Id();
//            databaseReference.child(Common.USER_NODE)
//                    .child(mCurrentUid)
//                    .addValueEventListener(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            databaseReference.child(Common.USER_NODE)
//                                    .child(mCurrentUid)
//                                    .child(Common.STATE_USER)
//                                    .onDisconnect()
//                                    .setValue(false);
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//                    });
//        }
    }

    private void checkNotification() {
        String userId = getIntent().getStringExtra("userId");
        if (userId != null) {
            addChildFragment(InfoUserFragment.newInstance(userId));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNotification();
    }

    //=======using to add child fragment from all fragment attached this activity=====
    public void addChildFragment(BaseFragment fragment) {
        addFragment(R.id.main_content, fragment);
    }

    public void openMenuLeft() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerview_menu_left);
        mainContent = findViewById(R.id.parent_content);
        containerFragment = findViewById(R.id.main_content);
        drawerLayout = findViewById(R.id.drawer_layout);

//        imgOpenLeftMenu = findViewById(R.id.openLeftMenu);
        imgPower = findViewById(R.id.power);
        imgAvatar = findViewById(R.id.imgAvatar);

        tvPreference = findViewById(R.id.tvPreference);
        tvDisplayName = findViewById(R.id.tvUser_name);
        tvEmail = findViewById(R.id.tvEmail);
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
    }

    private void event() {
        mAdapter.setOnOptionClickListner((v, position) -> {
            drawerLayout.closeDrawers();
            addChildFragment(mOptions.get(position).getContent());
        });

//        imgOpenLeftMenu.setOnClickListener(v -> drawerLayout.openDrawer(GravityCompat.START));

        drawerLayout.addDrawerListener(drawerToggle);

        imgPower.setOnClickListener(v -> {
            logOut();
        });

        imgAvatar.setOnClickListener(v -> {
            drawerLayout.closeDrawers();
            addChildFragment(new ProfileFragment());
        });

        tvPreference.setOnClickListener(v -> {
            drawerLayout.closeDrawers();
            startActivity(new Intent(this, PreferencesActivity.class));
        });
    }

    private void logOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setIcon(R.drawable.ic_logout)
                .setTitle(R.string.title_logout)
                .setMessage(R.string.message_logout)
                .setNegativeButton(R.string.cancel, (dialog, which) -> {
                    //
                })
                .setPositiveButton(R.string.log_out, (dialog, which) -> {
                    //
                    authManager.clearInfoCurrentUser();
                    switchActivity(LoginActivity.class, null);
                }).show();
    }

    private void initData() {
        initRecyclerView();
        mOptions = new ArrayList<>();
        mOptions.add(new OptionDrawer(R.drawable.ic_home, R.string.home, homeFragment));
        mOptions.add(new OptionDrawer(R.drawable.ic_help, R.string.help, helpFragment));
        mOptions.add(new OptionDrawer(R.drawable.ic_about, R.string.about, aboutFragment));

        mAdapter = new OptionDrawerAdapter(mOptions);
        recyclerView.setAdapter(mAdapter);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open_left_menu, R.string.close_left_menu) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                mainContent.setTranslationX(slideOffset * drawerView.getWidth());
                hideSoftInput();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                setInfoCurrentUser();
                homeFragment.setImgOpenMenuLeft(R.drawable.arrow_left_black);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                homeFragment.setImgOpenMenuLeft(R.drawable.ic_burger);
            }
        };

        presenter = new MainActivityPresenter(this);
    }

    public void setInfoCurrentUser() {
        if (authManager.infoUserAvailable()) {
            Glide.with(this)
                    .load(authManager.getUrlAvatar())
                    .into(imgAvatar);
            tvDisplayName.setText(authManager.getDisplayName());
            tvEmail.setText(authManager.getEmail());
        }
    }

    @Override
    protected void OnDestroy() {

    }

    @Override
    public void onBackPressed() {
        BaseFragment currentFragment = getCurrentFragmentOn(R.id.main_content);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            if (currentFragment != null) {
                if (currentFragment.onBackPressed()) {
                    super.onBackPressed();
                    return;
                }
            }
            pressBackAgain();
        }
    }

    private int count = 0;

    private void pressBackAgain() {
        count++;
        if (count == 1) {
            toast(getString(R.string.press_back_again));
            new CountDownTimer(3000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    count++;
                }

                @Override
                public void onFinish() {
                    count = 0;
                }
            }.start();
        } else {
            this.finish();
        }
    }

    //=========cancel loading=====
    @Override
    public void onCancel() {

    }

    //====network state=====
    @Override
    public void onLost() {
        showSnackBar(containerFragment,
                getString(R.string.internet_not_available)
                        + "\n"
                        + getString(R.string.offline_mode));
    }
}
