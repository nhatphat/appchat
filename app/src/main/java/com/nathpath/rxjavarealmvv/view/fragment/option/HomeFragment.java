package com.nathpath.rxjavarealmvv.view.fragment.option;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.adapter.ViewPagerAdapter;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.PageContent;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.callback.HomeFragmentView;
import com.nathpath.rxjavarealmvv.view.fragment.content.AllUserFragment;
import com.nathpath.rxjavarealmvv.view.fragment.content.ContactFragment;
import com.nathpath.rxjavarealmvv.view.fragment.content.FriendFragment;
import com.nathpath.rxjavarealmvv.view.fragment.content.RequestFragment;
import com.nathpath.rxjavarealmvv.view.fragment.error.ErrorFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HomeFragment extends BaseFragment
        implements HomeFragmentView, ViewPager.OnPageChangeListener {
    private AppBarLayout ablHome;
    private ImageView imgOpenMenuLeft;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private List<PageContent> mData;
    private ViewPagerAdapter mAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView(View rootView) {
        ablHome = rootView.findViewById(R.id.ablHome);
        imgOpenMenuLeft = rootView.findViewById(R.id.imgOpenMenuLeft);
        tabLayout = rootView.findViewById(R.id.home_tablayout);
        viewPager = rootView.findViewById(R.id.home_viewpager);
    }

    @Override
    protected void setData() {
        mData = new ArrayList<>();
        mData.add(new PageContent(
                R.drawable.ic_message_none,
                R.drawable.ic_message,
                R.drawable.white,
                R.string.message,
//                new FriendFragment()
                new AllUserFragment()
        ));
        mData.add(new PageContent(
                R.drawable.ic_friend_none,
                R.drawable.ic_friend,
                R.drawable.white,
                R.string.request,
                new RequestFragment()
        ));
        mData.add(new PageContent(
                R.drawable.ic_contact_none,
                R.drawable.ic_contact,
                R.drawable.white,
                R.string.contact,
                new ContactFragment()
        ));
        mData.add(new PageContent(
                R.drawable.ic_more_none,
                R.drawable.ic_more,
                R.drawable.white,
                R.string.more,
                new ErrorFragment()
        ));

        mAdapter = new ViewPagerAdapter(context, getChildFragmentManager(), mData);
        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);
        defaultForNonSelectedAndSelected(tabLayout, mData);
    }

    private void defaultForNonSelectedAndSelected(TabLayout tabLayout, List<PageContent> list) {
        int sl = tabLayout.getSelectedTabPosition();
        for (int i = 0; i < list.size(); i++) {
            if (i != sl) {
                setTabLayoutCustomView(Objects.requireNonNull(tabLayout.getTabAt(i)), mAdapter.getTabView(i, false));
            } else {
                setTabLayoutCustomView(Objects.requireNonNull(tabLayout.getTabAt(i)), mAdapter.getTabView(i, true));
                ablHome.setBackgroundResource(list.get(i).getBackgroundForAppbarLayout());
            }
        }
    }

    private void setTabLayoutCustomView(TabLayout.Tab tab, View customView) {
        tab.setCustomView(null);
        tab.setCustomView(customView);
    }

    @Override
    protected void event() {
        imgOpenMenuLeft.setOnClickListener(this);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgOpenMenuLeft:
                ((MainActivity) mActivity).openMenuLeft();
                break;
        }
    }

    public void setImgOpenMenuLeft(int resId){
        imgOpenMenuLeft.setImageResource(resId);
    }

    //=======viewpager listener========
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        defaultForNonSelectedAndSelected(tabLayout, mData);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
