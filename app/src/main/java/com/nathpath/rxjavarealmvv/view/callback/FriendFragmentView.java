package com.nathpath.rxjavarealmvv.view.callback;

import com.nathpath.rxjavarealmvv.data.model.User;

import java.util.List;

public interface FriendFragmentView {
    void onLoadFriendListSuccess(List<User> friendList, List<String> idList, boolean nulll);
    void onDeleteAllMessageSuccess();
    void onUnFriendSuccess(String userId);

    void onFailed(String err);
}
