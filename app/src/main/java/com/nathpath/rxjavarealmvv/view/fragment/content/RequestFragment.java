package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.adapter.RequestAdapter;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.presenter.content.RequestFragmentPresenter;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.callback.RequestFragmentView;

import java.util.ArrayList;
import java.util.List;

public class RequestFragment extends BaseFragment implements RequestFragmentView {
    private List<User> mData;
    private List<String> mId;
    private RequestAdapter mAdapter;
    private RequestFragmentPresenter mPresenter;
    private RecyclerView rcvRequest;
    private TextView noRequest;

    private String userIdSelected = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_request;
    }

    @Override
    protected void initView(View rootView) {
        rcvRequest = rootView.findViewById(R.id.rcvRequest);
        noRequest = rootView.findViewById(R.id.noRequest);
    }

    @Override
    protected void setData() {
        mPresenter = new RequestFragmentPresenter(this);

        mData = new ArrayList<>();
        mId = new ArrayList<>();
        mAdapter = new RequestAdapter(mData);
        rcvRequest.setHasFixedSize(true);
        rcvRequest.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false));
        rcvRequest.setAdapter(mAdapter);

        mPresenter.loadRequest(databaseReference, authManager);
    }

    @Override
    protected void event() {
        mAdapter.setOnItemClickListener((view, position) -> {
            if (view instanceof Button) {
                Button btn = (Button) view;
                userIdSelected = mId.get(position);
                Log.e("error", "index nhấn = " + userIdSelected);
                if (btn.getId() == R.id.btnAccept) {
                    mPresenter.cancelRequest(databaseReference, authManager, mId.get(position), true);
                } else {//btn cancel
                    mPresenter.cancelRequest(databaseReference, authManager, mId.get(position), false);
                }
            } else {
                ((MainActivity) mActivity).addChildFragment(
                        InfoUserFragment.newInstance(mId.get(position))
                );
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onLoadRequestSuccess(List<User> list, List<String> id, boolean nulll) {
        if (nulll) {
            mId.clear();
            mData.clear();
            mAdapter.notifyDataSetChanged();
            noRequest.setVisibility(View.VISIBLE);
        } else {
            if (list != null && id != null) {
                noRequest.setVisibility(View.GONE);
                mData.clear();
                mData.addAll(list);
                mId.clear();
                mId.addAll(id);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onAcceptRequestSuccess(String user_id) {
//        ((MainActivity) mActivity).addChildFragment(ConversationFragment.newInstance(user_id));
        toast(getString(R.string.became_friend));
    }

    @Override
    public void onCancelRequestSuccess(boolean becomeFriend) {
        if (becomeFriend) {
            Log.e("error", "index sau nhấn = " + userIdSelected);
            mPresenter.acceptRequest(databaseReference, authManager, userIdSelected);
        } else {
            toast(getString(R.string.cancelled));
        }
        userIdSelected = "";
        if (mData.size() != 0) {
            mAdapter.notifyDataSetChanged();
        } else {
            noRequest.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFailed(String err) {
        toast(err);
    }
}
