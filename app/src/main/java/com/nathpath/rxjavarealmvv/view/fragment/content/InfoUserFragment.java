package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.presenter.content.InfoUserFragmentPresenter;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.callback.InfoUserFragmentView;
import com.nathpath.rxjavarealmvv.view.fragment.profile.ProfileFragment;

import java.text.DateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class InfoUserFragment extends BaseFragment implements InfoUserFragmentView {
    private final int TALK = 1;
    private final int UN_FRIEND = 2;
    private final int YOUR_SELF = 3;
    private final int ADD_FRIEND = 4;
    private final int ACCEPT_REQUEST = 5;
    private final int CANCEL_REQUEST = 6;
    private final int CANCEL_ADD_FRIEND = 7;

    private TextView tvName;
    private TextView tvStatus;
    private Button btn_Submit;
    private Button btn_cancel;
    private ImageView imgBack;
    private CircleImageView civAvatar;

    private Bundle data;
    private static final String ARGUMENT_FRAGMENT_USER_ID = "user_id";

    private String user_id;

    private InfoUserFragmentPresenter mPresenter;

    public static InfoUserFragment newInstance(String user_id) {
        InfoUserFragment infoUserFragment = new InfoUserFragment();
        Bundle data = new Bundle();
        data.putString(ARGUMENT_FRAGMENT_USER_ID, user_id);
        infoUserFragment.setArguments(data);
        return infoUserFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_info_user;
    }

    @Override
    protected void initView(View rootView) {
        tvName = rootView.findViewById(R.id.tvName);
        tvStatus = rootView.findViewById(R.id.tvStatus);
        btn_Submit = rootView.findViewById(R.id.btn_Submit);
        btn_cancel = rootView.findViewById(R.id.btn_cancel);
        civAvatar = rootView.findViewById(R.id.civAvatar);
        imgBack = rootView.findViewById(R.id.imgBack);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getArguments();
    }

    @Override
    protected void setData() {
        mPresenter = new InfoUserFragmentPresenter(this);

        if (data != null) {
            user_id = data.getString(ARGUMENT_FRAGMENT_USER_ID);
            if (user_id != null) {
                mPresenter.confirmRelative(databaseReference, authManager, user_id);
                mPresenter.getInfoUser(databaseReference, user_id);
            }
        }
    }

    private void loadImageInto(String imageURL, ImageView imageView) {
        if (getActivity() != null) {
            Glide.with(this)
                    .load(imageURL)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.loading_large)
                    )
                    .into(imageView);
        }
    }

    private void isYourSelf() {
        btn_Submit.setEnabled(true);
        btn_Submit.setText(GetString(R.string.go_to_profile));
        btn_Submit.setTag(YOUR_SELF);
        btn_Submit.setVisibility(View.VISIBLE);

        btn_cancel.setVisibility(View.GONE);
    }

    private void isFriend() {
        btn_Submit.setEnabled(true);
        btn_Submit.setText(GetString(R.string.talk));
        btn_Submit.setTag(TALK);
        btn_Submit.setVisibility(View.VISIBLE);

        btn_cancel.setText(GetString(R.string.un_friend));
        btn_cancel.setTag(UN_FRIEND);
        btn_cancel.setVisibility(View.VISIBLE);
    }

    private void isRequest() {
        btn_Submit.setEnabled(true);
        btn_Submit.setText(GetString(R.string.accept_request));
        btn_Submit.setTag(ACCEPT_REQUEST);
        btn_Submit.setVisibility(View.VISIBLE);

        btn_cancel.setText(GetString(R.string.cancel_request));
        btn_cancel.setTag(CANCEL_REQUEST);
        btn_cancel.setVisibility(View.VISIBLE);
    }

    private void isSentRequest() {
        btn_Submit.setEnabled(false);
        btn_Submit.setText(GetString(R.string.send_request));
        btn_Submit.setVisibility(View.VISIBLE);

        btn_cancel.setText(GetString(R.string.cancel_request));
        btn_cancel.setTag(CANCEL_ADD_FRIEND);
        btn_cancel.setVisibility(View.VISIBLE);
    }

    private void noRelative() {
        btn_Submit.setEnabled(true);
        btn_Submit.setText(GetString(R.string.add_friend));
        btn_Submit.setTag(ADD_FRIEND);
        btn_Submit.setVisibility(View.VISIBLE);

        btn_cancel.setVisibility(View.GONE);
    }

    private String GetString(@StringRes int resId) {
        return context.getString(resId);
    }

    @Override
    protected void event() {
        btn_Submit.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Submit:
                switch ((int) btn_Submit.getTag()) {
                    case YOUR_SELF:
                        ((MainActivity) mActivity).addChildFragment(new ProfileFragment());
                        break;

                    case ADD_FRIEND:
                        mPresenter.addFriend(databaseReference, authManager, user_id);
                        break;

                    case ACCEPT_REQUEST:
                        mPresenter.cancelRequest(databaseReference, authManager, user_id, true);
                        break;

                    case TALK:
                        ((MainActivity) mActivity).addChildFragment(
                                ConversationFragment.newInstance(user_id)
                        );
                        break;
                }
                break;

            case R.id.btn_cancel:
                confirmBtnCancel();
                break;

            case R.id.imgBack:
                mActivity.onBackPressed();
                break;
        }
    }

    private void confirmBtnCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        String title = btn_cancel.getText().toString();
        builder.setIcon(R.drawable.ic_warning)
                .setTitle(title)
                .setMessage(getString(R.string.confirm_cancel, title))
                .setNegativeButton(R.string.no, (dialog, which) -> {
                    //
                })
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    switch ((int) btn_cancel.getTag()) {
                        case CANCEL_REQUEST:
                            mPresenter.cancelRequest(databaseReference, authManager, user_id, false);
                            break;

                        case CANCEL_ADD_FRIEND:
                            mPresenter.cancelSentRequest(databaseReference, authManager, user_id);
                            break;

                        case UN_FRIEND:
                            mPresenter.deleteFriendFromFriendList(databaseReference, authManager, user_id);
                            break;
                    }
                }).show();
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onGetInfoUserSuccess(User user) {
        if (user != null) {
            tvName.setText(user.getDisplay_name());
            tvStatus.setText(user.getStatus());
            loadImageInto(user.getAvatar(), civAvatar);
        }
    }

    @Override
    public void onConfirmRelativeSuccess(int relative) {
        switch (relative) {
            case 1:
                isYourSelf();
                break;
            case 2:
                isFriend();
                break;
            case 3:
                isRequest();
                break;
            case 4:
                isSentRequest();
                break;
            case 5:
                noRelative();
                break;
        }
    }

    @Override
    public void onAddFriendSuccess() {
        isSentRequest();
    }

    @Override
    public void onAddFriendToFriendListSuccess() {
        isFriend();
    }

    @Override
    public void onDeleteFriendToFriendListSuccess() {
        noRelative();

    }

    @Override
    public void onCancelRequestSuccess(boolean becomeFriend) {
        if (becomeFriend) {
            mPresenter.addFriendToFriendList(databaseReference, authManager, user_id);
        } else {
            noRelative();
        }
    }

    @Override
    public void onCancelSentRequestSuccess() {
        noRelative();
    }

    @Override
    public void onFailed(String err) {
        toast(err);
    }
}
