package com.nathpath.rxjavarealmvv.view.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.GeneralActivity;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.activity.login.LoginActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class SplashActivity extends GeneralActivity implements Animation.AnimationListener {
    private CircleImageView imgIcon_Splash;
    private TextView tvSlogan_Splash;

    private Animation slide_up;
    private Animation slide_loss;
    private Animation zoom_out_blink;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initView();

        if (firebaseAuth == null) {
            firebaseAuth = FirebaseAuth.getInstance();
        }
        if (firebaseAuth.getCurrentUser() != null) {
            Glide.with(this)
                    .load(authManager.getUrlAvatar())
                    .into(imgIcon_Splash);
        }

        setAnimation();
        slide_up.setAnimationListener(this);
        slide_loss.setAnimationListener(this);
        imgIcon_Splash.startAnimation(slide_up);
        tvSlogan_Splash.startAnimation(zoom_out_blink);
    }

    private void initView() {
        imgIcon_Splash = findViewById(R.id.imgIcon_splash);
        tvSlogan_Splash = findViewById(R.id.tvSlogan_splash);
    }

    private void setAnimation() {
        slide_up = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slide_loss = AnimationUtils.loadAnimation(this, R.anim.slide_loss);
        zoom_out_blink = AnimationUtils.loadAnimation(this, R.anim.zoom_out_blink);
    }

    //====event animation======

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation.equals(slide_up)) {
            imgIcon_Splash.startAnimation(slide_loss);
        }
        if (animation.equals(slide_loss)) {
            if (firebaseAuth.getCurrentUser() == null) {
                startActivity(new Intent(this, LoginActivity.class));
            } else {
                startActivity(new Intent(this, MainActivity.class));
            }
            finish();
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
