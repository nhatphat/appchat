package com.nathpath.rxjavarealmvv.view.activity.login;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseActivity;
import com.nathpath.rxjavarealmvv.presenter.login.LoginActivityPresenter;
import com.nathpath.rxjavarealmvv.services.AuthManager;
import com.nathpath.rxjavarealmvv.services.NetworkState;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.callback.LoginActivityView;

public class LoginActivity extends BaseActivity implements View.OnClickListener, LoginActivityView {
    private RelativeLayout mainLogin;
    private TextView tvTitleSignIn_SignUp;
    private TextView tvSignUp;
    private Button btnSubmit;
    private EditText edtDisplayName;
    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtPassword_Confirm;
    private TextInputLayout tilDisplayName;
    private TextInputLayout tilPassConf;
    private LinearLayout notMember;
    private ImageView imgBackToLogin;
    private ImageView imgExit;
    private boolean isLogin = true;
    private boolean interrupt = false;

    private LoginActivityPresenter presenter;

    private String username = "";
    private String password = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected void OnCreate() {

        receiver.registerReceiver(this);

        Init();
        initData();
        switchSignIn();
        event();
    }

    @Override
    protected void OnDestroy() {

    }

    private void Init() {
        mainLogin = findViewById(R.id.main_login);
        tvTitleSignIn_SignUp = findViewById(R.id.tvLogin);
        tvSignUp = findViewById(R.id.tvSign_up);
        btnSubmit = findViewById(R.id.btnSubmit);
        edtDisplayName = findViewById(R.id.edtDisplayName);
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        edtPassword_Confirm = findViewById(R.id.edtPassword_confirm);
        tilDisplayName = findViewById(R.id.tilDisplayName);
        tilPassConf = findViewById(R.id.tilPassConf);
        notMember = findViewById(R.id.not_member);
        imgBackToLogin = findViewById(R.id.imgBackToLogin);
        imgExit = findViewById(R.id.imgExit);
    }

    private void initData() {
        presenter = new LoginActivityPresenter(context, authManager, databaseReference, this);
    }

    private void event() {
        btnSubmit.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        imgBackToLogin.setOnClickListener(this);
        imgExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                interrupt = false;
                if (isLogin) {
                    submitSignIn();
                } else {
                    submitSignUp();
                }
                break;
            case R.id.tvSign_up:
                switchSignUp();
                break;
            case R.id.imgExit:
                exit();
                break;
            case R.id.imgBackToLogin:
                switchSignIn();
                break;
        }
    }

    private void submitSignIn() {
        username = edtUsername.getText().toString();
        password = edtPassword.getText().toString();
        if (checkUserPass(username, password)) {
            if (!NetworkState.isNetworkAvailable(this)) {
                switchLostNetwork();
                return;
            }
            showLoading(true);
            if (!interrupt) {
                presenter.login(username, password);
            }
        }
    }

    private void submitSignUp() {
        username = edtUsername.getText().toString();
        password = edtPassword.getText().toString();
        String displayName = edtDisplayName.getText().toString().trim();
        String confirm_pass = edtPassword_Confirm.getText().toString();
        if (checkUserPass(username, password)) {
            if (displayName.equals("")) {
                edtDisplayName.setError(getString(R.string.not_blank));
                return;
            }
            if (confirm_pass.equals("") || !confirm_pass.equals(password)) {
                edtPassword_Confirm.setError(getString(R.string.not_match));
                return;
            }
            if (!NetworkState.isNetworkAvailable(this)) {
                switchLostNetwork();
                return;
            }
            showLoading(true);
            if (!interrupt) {
                presenter.register(displayName, username, password);
            }
        }
    }

    private boolean checkUserPass(String user, String pass) {
        if (user.equals("")) {
            edtUsername.setError(getString(R.string.not_blank));
            return false;
        } else {
            String regex = "@gmail.com";
            if (!user.contains(regex)) {
                edtUsername.setError(getString(R.string.email_invalid));
                return false;
            } else {
                if (pass.equals("")) {
                    edtPassword.setError(getString(R.string.not_blank));
                    return false;
                } else {
                    if (pass.length() < 6) {
                        edtPassword.setError(getString(R.string.password_min_length));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void switchSignIn() {
        tvTitleSignIn_SignUp.setText(R.string.login);
        btnSubmit.setText(R.string.login);
        notMember.setVisibility(View.VISIBLE);
        isLogin = true;

        tilDisplayName.setVisibility(View.GONE);
        imgBackToLogin.setVisibility(View.GONE);
        tilPassConf.setVisibility(View.GONE);
    }

    private void switchSignUp() {
        tvTitleSignIn_SignUp.setText(R.string.sign_up);
        btnSubmit.setText(R.string.sign_up);
        tilDisplayName.setVisibility(View.VISIBLE);
        tilPassConf.setVisibility(View.VISIBLE);
        imgBackToLogin.setVisibility(View.VISIBLE);
        isLogin = false;

        notMember.setVisibility(View.GONE);
    }

    private void exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setIcon(R.drawable.back_door)
                .setTitle(R.string.exit)
                .setMessage(R.string.message_exit)
                .setNegativeButton(R.string.no, (dialog, which) -> {
                    //
                })
                .setPositiveButton(R.string.exit, (dialog, which) -> {
                    //
                    finish();
                }).show();
    }

    //=========cancel loading=====
    @Override
    public void onCancel() {
        interrupt = true;
    }

    //=========auth=========
    @Override
    public void signInSuccess() {
        showLoading(false);
        toast(getString(R.string.sign_in_success));

        Intent i = getBaseContext().getPackageManager().
                getLaunchIntentForPackage(getBaseContext().getPackageName());
        if (i != null) {
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(i);
        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void signInFailed(String err) {
        showLoading(false);
        toast(err);
    }

    @Override
    public void registerSuccess() {
        showLoading(false);
        toast(getString(R.string.sign_up_success));
        switchSignIn();
    }

    @Override
    public void registerFailed(String err) {
        showLoading(false);
        toast(err);
    }

    //=======connect state=====

    @Override
    public void onLost() {
        showSnackBar(mainLogin, getString(R.string.internet_not_available));
    }
}
