package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.data.model.UserViewHolder;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.squareup.picasso.Picasso;

public class AllUserFragment extends BaseFragment {

//    private SearchView svAllUser;
//    private ImageView imgExitAllUser;
    private RecyclerView rcvUser;
    private SwipeRefreshLayout refreshAllUser;

    private FirebaseRecyclerOptions options;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_all_user;
    }

    @Override
    protected void initView(View rootView) {
//        svAllUser = rootView.findViewById(R.id.svAllUser);
//        imgExitAllUser = rootView.findViewById(R.id.imgExitAllUser);
        rcvUser = rootView.findViewById(R.id.rcvUser);
        refreshAllUser = rootView.findViewById(R.id.refreshAllUser);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadAllUser();
    }

    private void loadAllUser() {
        FirebaseRecyclerAdapter<User, UserViewHolder> mAdapter = new FirebaseRecyclerAdapter<User, UserViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull UserViewHolder holder, int position, @NonNull User model) {
                holder.tvUserDislayName.setText(model.getDisplay_name());
                holder.tvUserStatus.setText(model.getStatus());
                loadImageInto(model.getThumb_avatar(), holder.civUserAvatar);
                loadImageInto(model.getState() ? R.drawable.ic_online : R.drawable.ic_offline, holder.imgUserState);

                holder.currentView.setOnClickListener(v -> {
                    ((MainActivity) mActivity).addChildFragment(InfoUserFragment.newInstance(getRef(position).getKey()));
                });
            }

            @NonNull
            @Override
            public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
                return new UserViewHolder(view);
            }
        };
        mAdapter.startListening();
        rcvUser.setAdapter(mAdapter);
    }

    //===== load image from url===========
    private void loadImageInto(String imageURL, ImageView imageView) {
        if (imageURL != null) {
            Glide.with(this)
                    .load(imageURL)
                    .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                    .into(imageView);
        }
    }

    //===== load image from resource=======
    private void loadImageInto(int imageID, ImageView imageView) {
        Glide.with(this)
                .load(imageID)
                .into(imageView);
    }

    @Override
    protected void setData() {
        rcvUser.setHasFixedSize(true);
        rcvUser.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));

        if (options == null) {
            options = new FirebaseRecyclerOptions.Builder<User>()
                    .setQuery(
                            databaseReference.child(Common.USER_NODE),
                            User.class
                    ).build();
        }
    }

    @Override
    protected void event() {
//        imgExitAllUser.setOnClickListener(this);
        refreshAllUser.setOnRefreshListener(() -> {
            refreshAllUser.setRefreshing(true);
            loadAllUser();
            refreshAllUser.setRefreshing(false);
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.imgExitAllUser:
//                mActivity.onBackPressed();
//                break;
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}

