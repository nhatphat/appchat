package com.nathpath.rxjavarealmvv.view.fragment.error;

import android.view.View;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;

public class ErrorFragment extends BaseFragment {
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_error;
    }

    @Override
    protected void initView(View rootView) {

    }

    @Override
    protected void setData() {

    }

    @Override
    protected void event() {

    }

    @Override
    public void onClick(View v) {

    }
}
