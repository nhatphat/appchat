package com.nathpath.rxjavarealmvv.view.activity.lost_network;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseActivity;
import com.nathpath.rxjavarealmvv.services.NetworkState;
import com.nathpath.rxjavarealmvv.view.activity.login.LoginActivity;

public class LostNetworkActivity extends BaseActivity {
    private Button btnTryAgain;
    private ImageView imgBackDoor;
    private RelativeLayout tryAgainContainer;
    private RelativeLayout tryingOnContainer;

    private Class toClass;
    private String activityLostNetwork;
    private Handler handler = new Handler();
    private Runnable runnable;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_lost_network;
    }

    @Override
    protected void OnCreate() {

        receiver.registerReceiver(this);

        initView();

        activityLostNetwork = getIntent().getStringExtra(BaseActivity.ACTIVITY_LOST_NETWORK);
        try {
            toClass = Class.forName(activityLostNetwork);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        runnable = () -> {
            if(NetworkState.isNetworkAvailable(this)){
                if(toClass != null){
                    startActivity(new Intent(this, toClass));
                }else{
                    startActivity(new Intent(this, LoginActivity.class));
                }
                finish();
            }else{
                tryAgain();
            }
        };

        btnTryAgain.setOnClickListener(v -> tryingOn());

        imgBackDoor.setOnClickListener(v -> finish());
    }

    private void initView(){
        btnTryAgain = findViewById(R.id.btnTryConnect);
        imgBackDoor = findViewById(R.id.imgBackDoor);
        tryAgainContainer = findViewById(R.id.tryAgain);
        tryingOnContainer = findViewById(R.id.tryingOn);
    }

    @Override
    protected void onStart() {
        super.onStart();

        tryAgain();
    }

    private void tryAgain(){
        tryAgainContainer.setVisibility(View.VISIBLE);
        tryingOnContainer.setVisibility(View.GONE);
    }

    private void tryingOn(){
        tryAgainContainer.setVisibility(View.GONE);
        tryingOnContainer.setVisibility(View.VISIBLE);
        handler.postDelayed(runnable, 700);
    }

    @Override
    protected void OnDestroy() {

    }

    @Override
    public void onAvailable() {
        super.onAvailable();
        tryingOn();
    }

    @Override
    public void onLost() {

    }

    @Override
    public void onCancel() {

    }
}
