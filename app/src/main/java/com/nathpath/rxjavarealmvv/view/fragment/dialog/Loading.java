package com.nathpath.rxjavarealmvv.view.fragment.dialog;

import android.app.Dialog;
import android.app.assist.AssistStructure;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseDialogFragment;

public class Loading extends BaseDialogFragment {
    private static onCancelListener listener;
    private Button btnCancel;

    @Override
    protected Dialog setDialog() {
        Dialog dialog = new Dialog(mActivity);
        Window window = dialog.getWindow();
        View view = View.inflate(mActivity, getLayoutResource(), null);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        if(window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            window.setBackgroundDrawableResource(R.color.trs_full);
        }

        return dialog;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_loading;
    }

    @Override
    protected void initView(View rootView) {
        btnCancel = rootView.findViewById(R.id.btnDialogCancel);
    }

    @Override
    protected void setData() {
        if(mActivity instanceof onCancelListener){
            listener = (onCancelListener) mActivity;
        }
    }

    @Override
    protected void event() {
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnCancel.getId()){
            if(listener != null){
                listener.onCancel();
                this.dismiss();
            }
        }
    }

    public interface onCancelListener{
        void onCancel();
    }
}
