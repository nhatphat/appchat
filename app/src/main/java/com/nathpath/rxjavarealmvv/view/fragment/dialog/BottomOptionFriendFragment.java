package com.nathpath.rxjavarealmvv.view.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseBottomDialogFragment;
import com.nathpath.rxjavarealmvv.data.model.User;

import de.hdodenhof.circleimageview.CircleImageView;

public class BottomOptionFriendFragment extends BaseBottomDialogFragment {
    private static final String ARG_USER = "arg_user";
    private static final String ARG_UID = "arg_uid";

    private CircleImageView civInfoUser;
    private TextView tvInfoUser;
    private RelativeLayout optionExit;
    private LinearLayout optionUnfriend;
    private LinearLayout optionDeleteMessage;

    private Bundle mData = null;
    private String userId;

    public static BottomOptionFriendFragment newInstance(User user, String userId){
        BottomOptionFriendFragment bottomDialog = new BottomOptionFriendFragment();
        Bundle data = new Bundle();
        data.putSerializable(ARG_USER, user);
        data.putString(ARG_UID, userId);
        bottomDialog.setArguments(data);
        bottomDialog.setCancelable(false);
        return bottomDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mData = getArguments();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected BottomSheetDialog setDialog() {
        BottomSheetDialog dialog = new BottomSheetDialog(mActivity, R.style.BottomSheetOption);
        View view = View.inflate(mActivity, getLayoutResource(), null);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        return dialog;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.bottom_dialog_option_friend;
    }

    @Override
    protected void initView(View rootView) {
        civInfoUser = rootView.findViewById(R.id.civInfoUser);
        tvInfoUser = rootView.findViewById(R.id.tvInfoUser);
        optionExit = rootView.findViewById(R.id.optionExit);
        optionUnfriend = rootView.findViewById(R.id.optionUnFriend);
        optionDeleteMessage = rootView.findViewById(R.id.optionDeleteMessage);
    }

    @Override
    protected void setData() {
        if(mData != null){
            User user = (User) mData.getSerializable(ARG_USER);
            userId = mData.getString(ARG_UID);
            if(user != null){
                tvInfoUser.setText(user.getDisplay_name());
                Glide.with(civInfoUser.getContext())
                        .load(user.getThumb_avatar())
                        .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                        .into(civInfoUser);
            }
        }
    }

    @Override
    protected void event() {
        optionExit.setOnClickListener(v -> {
            if(onOptionClickListener != null){
                onOptionClickListener.onOptionClick(optionExit, userId);
            }
            dismiss();
        });

        optionDeleteMessage.setOnClickListener(v -> {
            if(onOptionClickListener != null){
                onOptionClickListener.onOptionClick(optionDeleteMessage, userId);
            }
        });

        optionUnfriend.setOnClickListener(v -> {
            if(onOptionClickListener != null){
                onOptionClickListener.onOptionClick(optionUnfriend, userId);
            }
        });

    }

    //listener
    public interface OnOptionClickListener{
        void onOptionClick(View view, String userId);
    }

    private static OnOptionClickListener onOptionClickListener;

    public void setOnOptionClickListener(OnOptionClickListener listener){
        onOptionClickListener = listener;
    }
}
