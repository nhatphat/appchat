package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.view.View;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;

public class MoreFragment extends BaseFragment{
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_more;
    }

    @Override
    protected void initView(View rootView) {

    }

    @Override
    protected void setData() {

    }

    @Override
    protected void event() {

    }

    @Override
    public void onClick(View v) {

    }
}
