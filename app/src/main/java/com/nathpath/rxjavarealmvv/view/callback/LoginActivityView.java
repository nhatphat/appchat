package com.nathpath.rxjavarealmvv.view.callback;

public interface LoginActivityView {
    void signInSuccess();
    void signInFailed(String err);

    void registerSuccess();
    void registerFailed(String err);
}
