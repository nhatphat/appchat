package com.nathpath.rxjavarealmvv.view.callback;

import com.nathpath.rxjavarealmvv.data.model.User;

import java.util.List;

public interface RequestFragmentView {
    void onLoadRequestSuccess(List<User> list, List<String> id, boolean nulll);
    void onAcceptRequestSuccess(String user_id);
    void onCancelRequestSuccess(boolean becomFriend);
    void onFailed(String err);
}
