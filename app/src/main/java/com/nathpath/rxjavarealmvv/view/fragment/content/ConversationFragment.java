package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.nathpath.rxjavarealmvv.Manifest;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.adapter.MessageAdapter;
import com.nathpath.rxjavarealmvv.base.BaseFragment;
import com.nathpath.rxjavarealmvv.data.model.Message;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.presenter.content.ConversationFragmentPresenter;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.utils.TimeAgo;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.callback.ConversationFragmentView;
import com.nathpath.rxjavarealmvv.view.fragment.dialog.BottomOptionMessageFragment;
import com.theartofdev.edmodo.cropper.CropImage;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class ConversationFragment extends BaseFragment
        implements ConversationFragmentView, BottomOptionMessageFragment.OnOptionClickListener {
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 111;
    private static final String ARGUMENT_ID = "id";
    public static final String IMAGE = "image";
    public static final String TEXT = "text";

    private EditText edtMessageChatBar;
    private ImageView imgCamera;
    private ImageView imgEmoji;
    private ImageView imgSendChatBar;
    private ImageView imgExitConver;
    private TextView tvNoConver;
    private TextView tvNameConver;
    private TextView tvStateConver;
    private CircleImageView civAvatarConver;
    private SwipeRefreshLayout srlRefreshConver;
    private ConversationFragmentPresenter presenter;

    private List<Message> mData;
    private MessageAdapter adapter;
    private RecyclerView rcvConver;
    private String nodeDataMessage;

    private Bundle argument = new Bundle();
    private String partnerId;
    private String partnerAvatar;
    private BottomOptionMessageFragment bottomOptionMessageFragment;

    private static String oldUid = "";
    @SuppressLint("StaticFieldLeak")
    private static ConversationFragment instance = new ConversationFragment();

    public static ConversationFragment newInstance(String uid) {
        if (!oldUid.equals(uid)) {
            oldUid = uid;
            Bundle data = new Bundle();
            data.putString(ARGUMENT_ID, uid);
            instance.setArguments(data);
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        argument = getArguments();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (authManager.infoUserAvailable()) {
            String mCurrentUid = authManager.getU_Id();
            presenter.setTimeUserEnterAndExitConversation(databaseReference, nodeDataMessage, mCurrentUid);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_conversation;
    }

    @Override
    protected void initView(View rootView) {
        edtMessageChatBar = rootView.findViewById(R.id.edtMessageChatBar);
        imgCamera = rootView.findViewById(R.id.imgCamera);
        imgEmoji = rootView.findViewById(R.id.imgEmoji);
        imgSendChatBar = rootView.findViewById(R.id.imgSendChatBar);
        imgExitConver = rootView.findViewById(R.id.imgExitConversation);
        tvNoConver = rootView.findViewById(R.id.noConver);
        tvNameConver = rootView.findViewById(R.id.tvUserDisplayNameConver);
        tvStateConver = rootView.findViewById(R.id.tvStateConver);
        civAvatarConver = rootView.findViewById(R.id.civAvatarConver);
        srlRefreshConver = rootView.findViewById(R.id.refreshConversation);
        rcvConver = rootView.findViewById(R.id.rcvConversation);
    }

    @Override
    protected void setData() {
        presenter = new ConversationFragmentPresenter(this);

        if (argument != null) {
            partnerId = (String) argument.get(ARGUMENT_ID);
        }
        nodeDataMessage = createDataMessageNode(partnerId, authManager.getU_Id());

        presenter.loadInfoPartner(databaseReference, partnerId);
        presenter.checkStatePartner(databaseReference, nodeDataMessage, partnerId);

        mData = new ArrayList<>();
        adapter = new MessageAdapter(mData, databaseReference, partnerId);
        rcvConver.setHasFixedSize(true);
        rcvConver.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        rcvConver.setAdapter(adapter);

        presenter.loadMessages(databaseReference, nodeDataMessage);
    }

    private void scrollToPosition(RecyclerView recyclerView) {
        recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
    }

    //===== load image from url===========
    private void loadImageInto(String imageURL, ImageView imageView) {
        if (imageURL != null && getActivity() != null) {
            Glide.with(this)
                    .load(imageURL)
                    .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                    .into(imageView);
        } else {
            Log.e("error", "loadImage conversation fragment getActivity() null");
        }
    }

    public static String createDataMessageNode(String partnerId, String yourId) {
        if (yourId != null) {
            int compare = yourId.compareTo(partnerId);
            if (compare > 0) {
                return partnerId + yourId;
            } else if (compare < 0) {
                return yourId + partnerId;
            }
        }
        return null;
    }

    @Override
    protected void event() {
        srlRefreshConver.setOnRefreshListener(() -> {
            srlRefreshConver.setRefreshing(true);
            presenter.loadMessages(databaseReference, nodeDataMessage);
            srlRefreshConver.setRefreshing(false);
        });

        adapter.setOnItemClickListener(new MessageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Message message, int position) {
                switch (view.getId()) {
                    case R.id.civPartnerAvatar:
                        ((MainActivity) mActivity).addChildFragment(InfoUserFragment.newInstance(partnerId));
                        break;

                    case R.id.imgPartnerMessage:
                    case R.id.imgYourMessage:
                        toast(message.getMessage());
                        break;
                }
            }

            @Override
            public void onItemLongClick(View view, Message message, int position) {
                switch (view.getId()) {
                    case R.id.tvPartnerMessage:
                        showBottomOptionMessage(TEXT, message, partnerAvatar);
                        break;
                    case R.id.imgPartnerMessage:
                        showBottomOptionMessage(IMAGE, message, partnerAvatar);
                        break;

                    case R.id.tvYourMessage:
                        showBottomOptionMessage(TEXT, message, authManager.getUrlAvatar());
                        break;
                    case R.id.imgYourMessage:
                        showBottomOptionMessage(IMAGE, message, authManager.getUrlAvatar());
                        break;
                }
            }
        });

        edtMessageChatBar.setOnClickListener(this);
        civAvatarConver.setOnClickListener(this);
        imgSendChatBar.setOnClickListener(this);
        imgExitConver.setOnClickListener(this);
        imgCamera.setOnClickListener(this);
        imgEmoji.setOnClickListener(this);
    }

    private void download(String url) {
        if (!checkPermissionWriteStorage()) {
            permissionRequest();
        }
        if (checkPermissionWriteStorage()) {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDescription("Image");
            request.setTitle("Image");
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS + "/" + getString(R.string.app_name) + "/" + partnerId,
                    url.split("token=")[1] + ".png"
            );
            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            if (manager != null) {
                manager.enqueue(request);
            }
        } else {
            toast("vui lòng vào cài đặt hệ thống cấp quyền cho phép ứng dụng ghi vào bộ nhó");
        }
    }

    private void permissionRequest() {
        ActivityCompat.requestPermissions(mActivity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    private boolean checkPermissionWriteStorage() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void showBottomOptionMessage(String type, Message message, String user_avatar) {
        bottomOptionMessageFragment = BottomOptionMessageFragment.newInstance(type, message, user_avatar);
        bottomOptionMessageFragment.show(getChildFragmentManager(), "bottom_mess");
        bottomOptionMessageFragment.setOnOptionClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edtMessageChatBar:
                scrollToPosition(rcvConver);
                break;

            case R.id.civAvatarConver:
                ((MainActivity) mActivity).addChildFragment(InfoUserFragment.newInstance(partnerId));
                break;

            case R.id.imgExitConversation:
                mActivity.onBackPressed();
                break;

            case R.id.imgCamera:
                cropImage();
                break;

            case R.id.imgEmoji:
                toast("emoji");
                break;

            case R.id.imgSendChatBar:
                sendMessage(edtMessageChatBar.getText().toString(), setTime(new Date()));
                edtMessageChatBar.setText("");
                break;
        }
    }

    private void cropImage() {
        CropImage.activity()
                .start(mActivity.context, ConversationFragment.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri image = result.getUri();
                sendImageMessage(image, setTime(new Date()));
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                toast(getString(R.string.please_try_again));
            }
        }
    }

    private String setTime(Date date) {
        String REGEX = " - ";
        return DateFormat.getTimeInstance().format(date)
                + REGEX
                + DateFormat.getDateInstance(DateFormat.FULL).format(date);
    }

    private void sendMessage(String message, String time) {
        if (!message.equals("") && nodeDataMessage != null) {
            Message newMessage = new Message(
                    authManager.getU_Id(),
                    message,
                    time
            );
            presenter.sendMessage(databaseReference, nodeDataMessage, newMessage);
        }
    }

    private void sendImageMessage(Uri image, String time) {
        if (image != null && nodeDataMessage != null) {
            Message newMessage = new Message(
                    authManager.getU_Id(),
                    "",
                    time
            );
            presenter.sendImageMessage(databaseReference, nodeDataMessage, image, newMessage);
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (authManager.infoUserAvailable()) {
            String mCurrentUid = authManager.getU_Id();
            presenter.setTimeUserEnterAndExitConversation(databaseReference, nodeDataMessage, mCurrentUid);
        }
    }

    @Override
    public void onLoadMessageSuccess(List<Message> list) {
        if (list != null) {
            if (list.size() != 0) {
                noConversation(false);
                mData.clear();
                mData.addAll(list);
                adapter.notifyDataSetChanged();
                scrollToPosition(rcvConver);
            } else {
                noConversation(true);
            }
        }
    }

    private void noConversation(boolean yes) {
        if (yes) {
            tvNoConver.setVisibility(View.VISIBLE);
            rcvConver.setVisibility(View.GONE);
        } else {
            tvNoConver.setVisibility(View.GONE);
            rcvConver.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDeleteMessageSuccess() {
        toast(getString(R.string.deleted_message));
        presenter.loadMessages(databaseReference, nodeDataMessage);
    }

    @Override
    public void onDeleteImageMessageSuccess() {

    }

    @Override
    public void onLoadInfoPartnerSuccess(String name, String avatar) {
        tvNameConver.setText(name);
        loadImageInto(avatar, civAvatarConver);
        partnerAvatar = avatar;//keep value to show bottom option
    }

    @Override
    public void onCheckStatePartnerSuccess(long lastSeen) {
        String state = TimeAgo.getTimeAgo(lastSeen, context);
        if (state != null) {
            tvStateConver.setText(state);
        }
    }

    @Override
    public void onSendMessageSuccess() {

    }

    @Override
    public void onFailed(String err) {
        toast(err);
    }

    @Override
    public void onOptionClick(View view, String messageBody, String messageKey) {
        switch (view.getId()) {
            case R.id.optionExitMess:
                bottomOptionMessageFragment.dismiss();
                break;

            case R.id.optionCopyMess:
                ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("", messageBody);
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                }
                toast(getString(R.string.copied));
                bottomOptionMessageFragment.dismiss();
                break;

            case R.id.optionDeleteMess:
                presenter.deleteMessage(databaseReference, nodeDataMessage, messageKey);
                if (messageBody.contains(Common.BASE_IMAGE_URL)) {
                    presenter.deleteImageMessage(messageBody);
                }
                bottomOptionMessageFragment.dismiss();
                break;

            case R.id.optionDownloadImage:
                download(messageBody);
                bottomOptionMessageFragment.dismiss();
                break;

            case R.id.optionShareMess:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, messageBody);
                startActivity(Intent.createChooser(sharingIntent, "Share with"));
                bottomOptionMessageFragment.dismiss();
                break;
        }
    }
}
