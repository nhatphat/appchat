package com.nathpath.rxjavarealmvv.view.fragment.option;

import android.view.View;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;

public class HelpFragment extends BaseFragment {

    public HelpFragment() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_help;
    }

    @Override
    protected void initView(View rootView) {

    }

    @Override
    protected void setData() {

    }

    @Override
    protected void event() {

    }

    @Override
    public void onClick(View v) {

    }
}
