package com.nathpath.rxjavarealmvv.view.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseBottomDialogFragment;
import com.nathpath.rxjavarealmvv.data.model.Message;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.view.fragment.content.ConversationFragment;

import de.hdodenhof.circleimageview.CircleImageView;

public class BottomOptionMessageFragment extends BaseBottomDialogFragment {
    private static final String ARG_TYPE = "arg_type";
    private static final String ARG_MESS = "arg_mess";
    private static final String ARG_AVATAR = "arg_avatar";

    private CircleImageView civInfoUserMess;
    private TextView tvInfoUserMess;
    private ImageView imgInfoUserMess;
    private RelativeLayout optionExit;
    private LinearLayout optionCopy;
    private LinearLayout optionDeleteMess;
    private LinearLayout optionDownloadImage;
    private LinearLayout optionShare;

    private Bundle mData = null;
    private String messageBody;
    private String messageKey;

    public static BottomOptionMessageFragment newInstance(String type, Message message, String user_avatar) {
        BottomOptionMessageFragment bottomDialog = new BottomOptionMessageFragment();
        Bundle data = new Bundle();
        data.putSerializable(ARG_MESS, message);
        data.putString(ARG_AVATAR, user_avatar);
        data.putString(ARG_TYPE, type);
        bottomDialog.setArguments(data);
        bottomDialog.setCancelable(false);
        return bottomDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mData = getArguments();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected BottomSheetDialog setDialog() {
        BottomSheetDialog dialog = new BottomSheetDialog(mActivity, R.style.BottomSheetOption);
        View view = View.inflate(mActivity, getLayoutResource(), null);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        return dialog;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.bottom_dialog_option_message;
    }

    @Override
    protected void initView(View rootView) {
        civInfoUserMess = rootView.findViewById(R.id.civInfoUserMess);
        tvInfoUserMess = rootView.findViewById(R.id.tvInfoUserMess);
        imgInfoUserMess = rootView.findViewById(R.id.imgInfoUserMess);
        optionExit = rootView.findViewById(R.id.optionExitMess);
        optionCopy = rootView.findViewById(R.id.optionCopyMess);
        optionDeleteMess = rootView.findViewById(R.id.optionDeleteMess);
        optionDownloadImage = rootView.findViewById(R.id.optionDownloadImage);
        optionShare = rootView.findViewById(R.id.optionShareMess);
    }

    @Override
    protected void setData() {
        if (mData != null) {
            Message message = (Message) mData.getSerializable(ARG_MESS);
            String avatar = (String) mData.get(ARG_AVATAR);
            String type = (String) mData.get(ARG_TYPE);
            if (message != null) {
                messageKey = message.getKey();
                messageBody = message.getMessage();
                tvInfoUserMess.setText(messageBody);
                Glide.with(civInfoUserMess.getContext())
                        .load(avatar)
                        .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                        .into(civInfoUserMess);

                assert type != null;
                if (type.equals(ConversationFragment.IMAGE)) {
                    Glide.with(imgInfoUserMess.getContext())
                            .load(messageBody)
                            .apply(new RequestOptions().placeholder(R.drawable.loading_large))
                            .into(imgInfoUserMess);
                    imgInfoUserMess.setVisibility(View.VISIBLE);
                    tvInfoUserMess.setVisibility(View.GONE);
                    optionDownloadImage.setVisibility(View.VISIBLE);
                } else {
                    imgInfoUserMess.setVisibility(View.GONE);
                    tvInfoUserMess.setVisibility(View.VISIBLE);
                    optionDownloadImage.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    protected void event() {
        setEvent(optionExit, messageBody, messageKey);
        setEvent(optionDeleteMess, messageBody, messageKey);
        setEvent(optionShare, messageBody, messageKey);
        setEvent(optionCopy, messageBody, messageKey);
        setEvent(optionDownloadImage, messageBody, messageKey);
    }

    private void setEvent(View view, String messageBody, String messageKey) {
        if (onOptionClickListener != null) {
            view.setOnClickListener(v -> onOptionClickListener.onOptionClick(view, messageBody, messageKey));
        }
    }

    //listener
    public interface OnOptionClickListener {
        void onOptionClick(View view, String messageBody, String messageKey);
    }

    private static OnOptionClickListener onOptionClickListener;

    public void setOnOptionClickListener(OnOptionClickListener listener) {
        onOptionClickListener = listener;
    }
}
