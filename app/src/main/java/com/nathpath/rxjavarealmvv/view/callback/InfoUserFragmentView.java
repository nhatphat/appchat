package com.nathpath.rxjavarealmvv.view.callback;

import com.nathpath.rxjavarealmvv.data.model.User;

public interface InfoUserFragmentView {
    void onGetInfoUserSuccess(User user);

    void onConfirmRelativeSuccess(int relative);

    void onAddFriendSuccess();

    void onAddFriendToFriendListSuccess();

    void onDeleteFriendToFriendListSuccess();

    void onCancelRequestSuccess(boolean becomeFriend);

    void onCancelSentRequestSuccess();

    void onFailed(String err);

}
