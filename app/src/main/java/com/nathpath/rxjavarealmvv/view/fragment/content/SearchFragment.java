package com.nathpath.rxjavarealmvv.view.fragment.content;

import android.view.View;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseFragment;

public class SearchFragment extends BaseFragment {
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_search;
    }

    @Override
    protected void initView(View rootView) {

    }

    @Override
    protected void setData() {

    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    protected void event() {

    }

    @Override
    public void onClick(View v) {

    }
}
