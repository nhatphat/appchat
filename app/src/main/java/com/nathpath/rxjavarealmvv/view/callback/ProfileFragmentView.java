package com.nathpath.rxjavarealmvv.view.callback;

public interface ProfileFragmentView {
    void onUpdateSuccess(String s);
    void onUpdateFailed(String e);
}
