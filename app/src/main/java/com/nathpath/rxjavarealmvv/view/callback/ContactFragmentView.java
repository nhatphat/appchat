package com.nathpath.rxjavarealmvv.view.callback;

import com.nathpath.rxjavarealmvv.data.model.Contact;

import java.util.List;

public interface ContactFragmentView {
    void loadContactSuccess(List<Contact> list);
    void loadContactFailed(String err);
}
