package com.nathpath.rxjavarealmvv.view.activity.preferences;

import android.app.Fragment;
import android.support.design.widget.AppBarLayout;
import android.widget.ImageView;
import android.widget.Switch;

import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.base.BaseActivity;
import com.nathpath.rxjavarealmvv.view.fragment.preferences.PreferencesFragment;

import java.util.Random;

public class PreferencesActivity extends BaseActivity {
    private ImageView imgExitPreference;
    private Switch swSetDefaultSetting;
    private AppBarLayout ablPre;

    private final int[] colorAppbar = {
            R.drawable.gradient_lemon_drizzl,
            R.drawable.gradient_luscious_lime,
            R.drawable.gradient_purple_lake,
            R.drawable.gradient_sanguine,
            R.drawable.gradient_ocean_blue,
            R.drawable.gradient_green
    };

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_preferences;
    }

    @Override
    protected void OnCreate() {
        receiver.registerReceiver(this);
        initPreference();
        initView();
        event();
        ablPre.setBackgroundResource(colorAppbar[new Random().nextInt(colorAppbar.length)]);
    }

    private void initView() {
        imgExitPreference = findViewById(R.id.imgExitPreference);
        swSetDefaultSetting = findViewById(R.id.swSetDefaultSetting);
        ablPre = findViewById(R.id.ablPre);
    }

    private void event() {
        imgExitPreference.setOnClickListener(v -> {
            onBackPressed();
        });

        swSetDefaultSetting.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                toast("da dat ve default");
            } else {
                toast("k default");
            }
        });

    }

    //init for preference
    private void initPreference() {
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment oldFragment = getFragmentManager().findFragmentById(R.id.preference);
        if (oldFragment == null) {
            transaction.add(R.id.preference, new PreferencesFragment()).commit();
        }
    }

    @Override
    protected void OnDestroy() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onLost() {
        toast(getString(R.string.internet_not_available));
    }

    @Override
    public void onCancel() {

    }
}
