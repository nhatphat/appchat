package com.nathpath.rxjavarealmvv.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.data.model.User;
import com.nathpath.rxjavarealmvv.utils.Common;
import com.nathpath.rxjavarealmvv.view.activity.MainActivity;
import com.nathpath.rxjavarealmvv.view.fragment.content.InfoUserFragment;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    private static final String CHANNEL = "channel";
    private static final int REQUEST_NOTIFICATION = 112;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getNotification() != null) {
            Log.e("error", "notification title: " + remoteMessage.getNotification().getTitle());
            Log.e("error", "notification body: " + remoteMessage.getNotification().getBody());
            Log.e("error", "notification body: " + remoteMessage.getData().get("userId"));
            showNotification(remoteMessage);
        } else {
            Log.e("onMessageReceived", "data null");
        }
    }

    private void showNotification(RemoteMessage remoteMessage) {
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("userId", remoteMessage.getData().get("userId"));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                REQUEST_NOTIFICATION,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL);
        builder.setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_app)
                .setSound(sound);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(REQUEST_NOTIFICATION, builder.build());
        }
    }
}
