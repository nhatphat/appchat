package com.nathpath.rxjavarealmvv.services;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.cantrowitz.rxbroadcast.RxBroadcast;
import io.reactivex.disposables.Disposable;

public class NetworkState {
    private static NetworkState networkState = new NetworkState();
    private Disposable disposable;
    public boolean isRegister = false;
    private NetworkStateListener listener;

    public static NetworkState getInstance(){
        return networkState;
    }

    public void registerReceiver(Context context) {
        if(context instanceof NetworkStateListener){
            listener = (NetworkStateListener) context;
        }
        if (disposable == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            disposable = RxBroadcast.fromBroadcast(context, intentFilter)
                                     .map(intent -> {
                                         if(isNetworkAvailable(context)){
                                             if(listener != null){
                                                 listener.onAvailable();
                                             }
                                         }else{
                                             if(listener != null){
                                                 listener.onLost();
                                             }
                                         }
                                         isRegister = true;
                                         return true;
                                     })
                                    .subscribe();
        }
    }

    public void unRegisterReceiver(){
        if(disposable != null && !disposable.isDisposed()){
            disposable.dispose();
            isRegister = false;
        }
    }

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if(connectivityManager != null){
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.isConnected();
    }

    public interface NetworkStateListener{
        void onAvailable();
        void onLost();
    }
}