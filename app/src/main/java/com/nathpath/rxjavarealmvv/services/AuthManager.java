package com.nathpath.rxjavarealmvv.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nathpath.rxjavarealmvv.R;
import com.nathpath.rxjavarealmvv.utils.Common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import id.zelory.compressor.Compressor;

public class AuthManager {
    private static final String INFO_CURRENT_USER = "info_current_user";
    private static final String UID = "uid";
    private static final String TOKEN = "token";
    private static final String EMAIL = "email";
    private static final String STATUS = "status";
    private static final String URL_AVATAR = "url_avatar";
    private static final String DISPLAY_NAME = "display_name";

    @SuppressLint("StaticFieldLeak")
    private static AuthManager authManager;
    private static OnUpdateStateListener onUpdateStateListener;

    private Context context;
    private FirebaseAuth firebaseAuth;
    private SharedPreferences preferences;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;

    private AuthManager(Context context, DatabaseReference databaseReference) {
        this.context = context;
        this.databaseReference = databaseReference;
        this.storageReference = FirebaseStorage.getInstance().getReference();
        this.preferences = context.getSharedPreferences(INFO_CURRENT_USER, Context.MODE_PRIVATE);
        this.firebaseAuth = FirebaseAuth.getInstance();
    }

    public static AuthManager getInstance(Context context, DatabaseReference databaseReference) {
        if (authManager == null) {
            authManager = new AuthManager(context, databaseReference);
        }
        return authManager;
    }

    public void saveInfoCurrentUser(FirebaseUser user) {
        user.getIdToken(true).addOnCompleteListener(task1 -> {
            saveUid(user.getUid());
            saveToken(task1.getResult().getToken());
            saveEmail(user.getEmail());
            saveFromDataBase(databaseReference.child(Common.USER_NODE).child(user.getUid()));
        });
    }

    private void saveUid(String uid) {
        savePreferenceValue(UID, uid);
    }

    private void saveToken(String token) {
        savePreferenceValue(TOKEN, token);
    }

    private void saveEmail(String email) {
        savePreferenceValue(EMAIL, email);
    }

    private void saveStatus(String status) {
        savePreferenceValue(STATUS, status);
    }

    private void saveUrlAvatar(String url) {
        savePreferenceValue(URL_AVATAR, url);
    }

    private void saveDisplayName(String name) {
        savePreferenceValue(DISPLAY_NAME, name);
    }

    private void savePreferenceValue(String key, String value) {
        if (preferences != null) {
            preferences.edit().putString(key, value).apply();
        }
    }

    public String getU_Id() {
        return getPreferenceValue(UID);
    }

    public String getToken() {
        return getPreferenceValue(TOKEN);
    }

    public String getEmail() {
        return getPreferenceValue(EMAIL);
    }

    public String getStatus() {
        return getPreferenceValue(STATUS);
    }

    public String getUrlAvatar() {
        return getPreferenceValue(URL_AVATAR);
    }

    public String getDisplayName() {
        return getPreferenceValue(DISPLAY_NAME);
    }

    private String getPreferenceValue(String key) {
        if (preferences != null) {
            return preferences.getString(key, key.equals(URL_AVATAR) ? Common.AVATAR_DEFAULT : null);
        }
        return null;
    }

    public void updateInfoCurrentUser(@Nullable String displayName, @Nullable String status, @Nullable Uri uriPhoto) {
        if (databaseReference != null && storageReference != null) {
            if (displayName != null) {
                setValueOnDataBaseUserNode(Common.DISPLAY_NAME_USER, displayName);
                saveDisplayName(displayName);//save in local
            }
            if (status != null) {
                setValueOnDataBaseUserNode(Common.STATUS_USER, status);
                saveStatus(status);//save in local
            }
            if (uriPhoto != null) {
                File thumb_path = new File(uriPhoto.getPath());
                ByteArrayOutputStream thumb_byte = new ByteArrayOutputStream();
                try {
                    Bitmap thumb_image = new Compressor(context)
                            .setMaxWidth(100)
                            .setMaxHeight(100)
                            .setQuality(75)
                            .compressToBitmap(thumb_path);
                    thumb_image.compress(Bitmap.CompressFormat.JPEG, 100, thumb_byte);
                } catch (IOException e) {
                    //
                    Log.e("error", "AuthManager updateInfoCurrentUser: compress thumb image failed");
                }

                StorageReference image_path = storageReference
                        .child(Common.PROFILE_IMAGE)
                        .child(getU_Id())
                        .child(Common.AVATAR_PNG);
                StorageReference thumb_image_path = storageReference
                        .child(Common.PROFILE_IMAGE)
                        .child(getU_Id())
                        .child(Common.THUMB_AVATAR_PNG);

                //save original image
                image_path.putFile(uriPhoto).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        image_path.getDownloadUrl().addOnSuccessListener(uri -> {
                            setValueOnDataBaseUserNode(Common.AVATAR_USER, uri.toString());
                            saveUrlAvatar(uri.toString());//save in local
                        });
                    } else {
                        Log.e("error", "save original image failed");
                    }
                });

                //save thumb image
                if (thumb_byte.toByteArray().length > 0) {
                    UploadTask uploadTask = thumb_image_path.putBytes(thumb_byte.toByteArray());
                    uploadTask.addOnCompleteListener(task1 -> {
                        if (task1.isSuccessful()) {
                            thumb_image_path.getDownloadUrl().addOnSuccessListener(uri -> {
                                setValueOnDataBaseUserNode(Common.THUMB_AVATAR_USER, uri.toString());
                            });
                        } else {
                            Log.e("error", "save thumb image failed");
                        }
                    });
                }
            }
            updateState(true);
        } else {
            updateState(false);
        }
    }

    private void updateState(boolean isSuccess) {
        if (onUpdateStateListener != null) {
            if (isSuccess) {
                onUpdateStateListener.onUpdateSuccess();
            } else {
                onUpdateStateListener.onUpdateFailed();
            }
        }
    }

    private void setValueOnDataBaseUserNode(String child, String value) {
        if (infoUserAvailable()) {
            databaseReference.child(Common.USER_NODE)
                    .child(getU_Id())
                    .child(child)
                    .setValue(value);
        }
    }

    private void saveFromDataBase(DatabaseReference path) {
        path.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                saveDisplayName(Objects.requireNonNull(dataSnapshot.child(Common.DISPLAY_NAME_USER).getValue()).toString());
                saveUrlAvatar(Objects.requireNonNull(dataSnapshot.child(Common.AVATAR_USER).getValue()).toString());
                saveStatus(Objects.requireNonNull(dataSnapshot.child(Common.STATUS_USER).getValue()).toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setUserState(boolean isOnline) {
        if (infoUserAvailable()) {
            databaseReference.child(Common.USER_NODE)
                    .child(getU_Id())
                    .child(Common.STATE_USER)
                    .setValue(isOnline);
        }
    }

    //===== update state==================
    public interface OnUpdateStateListener {
        void onUpdateSuccess();

        void onUpdateFailed();
    }

    public void setOnUpdateStateListener(OnUpdateStateListener listener) {
        onUpdateStateListener = listener;
    }
    //=====================================

    public boolean infoUserAvailable() {
        //try refresh preference
        if(getU_Id() == null){
            preferences = null;
            preferences = context.getSharedPreferences(INFO_CURRENT_USER, Context.MODE_PRIVATE);
        }
        return getU_Id() != null;
//                && getToken() != null
//                && getStatus() != null
//                && getEmail() != null
//                && getDisplayName() != null;
    }

    public void clearInfoCurrentUser() {
        setUserState(false);
        if (firebaseAuth != null) {
            firebaseAuth.signOut();
        }
        if (preferences != null) {
            preferences.edit().clear().apply();
            preferences = null;
        }
        authManager = null;
    }
}
